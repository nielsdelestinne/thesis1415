package niels.master.thesis.prototypev2.aws.sqs;

import java.util.ArrayList;
import java.util.List;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.activities.func2.FragmentsGroupAdapter;
import niels.master.thesis.prototypev2.activities.func2.StoryboardPlayingFragmentsActivity;
import niels.master.thesis.prototypev2.entities.StoryboardFragment;
import niels.master.thesis.prototypev2.lib.FancyCoverFlow;
import android.app.Dialog;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;
import com.jess.ui.TwoWayGridView;

/**
 * Class for receiving SQS messages
 * @author Niels
 *
 */
public class MySQS2 {

	// Bad practice authentication...
	private static final String ACCESS_KEY_ID = ""; // PRIVATE
	private static final String SECRET_KEY = ""; // PRIVATE
	private static final String QUEUE_URL = "https://sqs.eu-west-1.amazonaws.com/169314756164/appqueue";
	private static final String QUEUE_URL_SENDING = "https://sqs.eu-west-1.amazonaws.com/169314756164/playerqueue";
	
	private AmazonSQSClient sqsClient;
	private FancyCoverFlow fancyCoverFlow;
	private TextView txtStatus;
	private FragmentsGroupAdapter adapter;
	private boolean active = true;
	
	/**
	 * Initializes the SQS client
	 * used for sending messages
	 */
	public MySQS2() {
		AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_KEY);		
		sqsClient = new AmazonSQSClient( credentials );
		sqsClient.setRegion(Region.getRegion(Regions.EU_WEST_1));
	}
	
	/**
	 * Initializes the SQS client
	 * Used for polling the queue
	 * @param textView 
	 * @param storyboardPlayingActivity 
	 * @param fragments 
	 */
	public MySQS2(FancyCoverFlow fancyCoverFlow,  FragmentsGroupAdapter adapter, TextView txtStatus) {
		AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_KEY);		
		sqsClient = new AmazonSQSClient( credentials );
		sqsClient.setRegion(Region.getRegion(Regions.EU_WEST_1));
		this.fancyCoverFlow = fancyCoverFlow;
		this.txtStatus = txtStatus;
		this.adapter = adapter;

	}
	
	/**
     * send a single message to the sqs queue
     * @param message
     */
    public void sendMessageToQueue(String message){
        SendMessageResult messageResult =  sqsClient.sendMessage(new SendMessageRequest(QUEUE_URL_SENDING, message));
        System.out.println("Message send result: " + messageResult.toString());
    }
	
	/**
	 * Method for receiving messages from the SQS queue
	 * @return
	 */
	public void receiveMessage() {
		new PollTheQueue().execute("");
	}
	
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * We poll the queue while using an AsyncTask, We poll the queue
	 * for a certain amount of time. If we would not do this with an
	 * asynctaks, the main thread would freeze and wait the full amount of
	 * time we are polling. By creating a asynctask, the main thread can continue
	 * while the queue is polled in the background.
	 * @author Niels
	 *
	 */
	private class PollTheQueue extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			// Receive messages
	        System.out.println("Receiving messages from MyQueue.\n");
	        ReceiveMessageRequest rmr = new ReceiveMessageRequest(QUEUE_URL);
	        rmr.setMaxNumberOfMessages(1);
	        // We keep listening for 20 seconds
	        rmr.setWaitTimeSeconds(2);
	        ReceiveMessageResult result = sqsClient.receiveMessage(rmr);
	        List<Message> messages = result.getMessages();
	        String resultingOutput = "";
	        for (Message message : messages) {
	        	// STORED IN RECEIVED MESSAGES
	        	//StoryboardPlayingActivity.updateGUI(message.getBody());
	        	//StoryboardPlayingActivity.receivedMessages.add(message.getBody());
				resultingOutput = message.getBody();
				// delete message
				String messageRecieptHandle = message.getReceiptHandle();
	            sqsClient.deleteMessage(new DeleteMessageRequest(QUEUE_URL, messageRecieptHandle));
			}
	        return resultingOutput;
		}
		
		@Override
        protected void onPostExecute(String message) {
            //TextView txt = (TextView) findViewById(R.id.output);
            //txt.setText("Executed"); // txt.setText(result);
            //System.out.println(result);
            updateGUI(message);
            if(isActive())
            	receiveMessage();
        }
		
		private final static String MESSAGE_PLAY = "PLAY_ACTIVATED";
		private final static String MESSAGE_PAUSE = "PAUSE_ACTIVATED";
		private final static String MESSAGE_PLAYLIST_START_ITEM = "PLAYLIST_START_ITEM,";
		private final static String MESSAGE_PLAYLIST_END = "PLAYLIST_END";
		
		private void updateGUI(String message){
			if(message.contains(MESSAGE_PLAYLIST_START_ITEM)) {
				System.out.println(message);
				String indexStr = message.replaceAll(MESSAGE_PLAYLIST_START_ITEM, "");
				int index = Integer.parseInt(indexStr);
				// Set the active index!
				StoryboardPlayingFragmentsActivity.active_index = index;
				// refresh
				adapter.refresh();
				// Scroll to index
				fancyCoverFlow.setSelection(index);
				//fancyCoverFlow.invalidate();
				System.out.println("PROCESSED NEXT ITEM!");
			}
			if(message.contains(MESSAGE_PLAY)) {
				System.out.println(message);
				txtStatus.setText("Status: Playing");
			}
			if(message.contains(MESSAGE_PAUSE)) {
				System.out.println(message);
				txtStatus.setText("Status: Paused");
			}
			if(message.contains(MESSAGE_PLAYLIST_END)) {
				System.out.println(message);
				// Set the active index!
				StoryboardPlayingFragmentsActivity.active_index = -1;
				// refresh
				adapter.refresh();
				txtStatus.setText("Status: Ended");
			}
		}
		
		private void infoDialog(StoryboardFragment storyboardFragment){
			// custom dialog
			final Dialog dialog = new Dialog(StoryboardPlayingFragmentsActivity.CONTEXT);
			dialog.setContentView(R.layout.dialog_show_information);
			dialog.setTitle(storyboardFragment.getTitle());

			// set the custom dialog components - text, image and button
			TextView text = (TextView) dialog.findViewById(R.id.text);
			text.setText(storyboardFragment.getDescription());
			ImageView imageView = (ImageView) dialog.findViewById(R.id.image);
			setImageForImageView(imageView, storyboardFragment.getImage());

			Button dialogButton = (Button) dialog.findViewById(R.id.btnClose);
			// if button is clicked, close the custom dialog
			dialogButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			dialog.show();
			
		}

		/**
		 * Helper method for loading in an image from
		 * the assets folder and setting it as the src
		 * for the provided ImageView object.
		 * @param imageView
		 * @param imageUrl
		 */
		private void setImageForImageView(ImageView imageView, String imageUrl) {
			imageUrl = imageUrl.substring(0, imageUrl.lastIndexOf('.'));
			int resID = StoryboardPlayingFragmentsActivity.CONTEXT.getResources().getIdentifier(imageUrl, "drawable",  StoryboardPlayingFragmentsActivity.CONTEXT.getPackageName());
			imageView.setImageResource(resID);
		}
		
	}
	
	
}
