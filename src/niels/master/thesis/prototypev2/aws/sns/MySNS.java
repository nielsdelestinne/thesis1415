package niels.master.thesis.prototypev2.aws.sns;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;

/**
 * Class for publishing SNS messages
 * @author Niels
 *
 */
public class MySNS {

	// Bad practice authentication...
	private static final String ACCESS_KEY_ID = ""; // PRIVATE
	private static final String SECRET_KEY = ""; // PRIVATE
	private static final String TOPIC_ARN_SEND = "arn:aws:sns:eu-west-1:169314756164:video_stream";
	private AmazonSNSClient snsClient;
	
	/**
	 * Initializes the SNS client
	 */
	public MySNS() {
		AWSCredentials credentials = new BasicAWSCredentials(ACCESS_KEY_ID, SECRET_KEY);		
		snsClient = new AmazonSNSClient( credentials );
		snsClient.setRegion(Region.getRegion(Regions.EU_WEST_1));
	}
	
	/**
	 * Publish message using SNS
	 * @param subject: subject of the message
	 * @param message: content of the message
	 */
	public void publish(String subject, String message) {
		//publish to an SNS topic
		PublishRequest publishRequest = new PublishRequest(TOPIC_ARN_SEND, message, subject);
		//PublishRequest publishRequest = new PublishRequest();
		//publishRequest.withTargetArn("arn:aws:sns:eu-west-1:169314756164:video_stream:2dedb589-2ee0-4e0a-8655-39161121d726");
		//publishRequest.withMessage(message);
		//publishRequest.withSubject(subject);
		PublishResult publishResult = snsClient.publish(publishRequest);
		//print MessageId of message published to SNS topic
		System.out.println("MessageId - " + publishResult.getMessageId());
	}
	
}
