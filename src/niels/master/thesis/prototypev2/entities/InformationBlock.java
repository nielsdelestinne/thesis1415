package niels.master.thesis.prototypev2.entities;

import java.util.Comparator;

import android.util.Log;

public abstract class InformationBlock implements Comparable<InformationBlock> {

	public static final String[] TABLES = {"fact_block", "image_block", "map_block", "quiz_block"};
	
	private int id;
	private String type;
	private String title;
	private String text;
	private int startTime;
	private int doc_id; //foreign key
	
	public int getId() {
		return id;
	}
	protected void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	protected void setType(String type) {
		this.type = type;
	}
	public String getTitle() {
		return title;
	}
	protected void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	protected void setText(String text) {
		this.text = text;
	}
	public int getStartTime() {
		return startTime;
	}
	protected void setStartTime(int startTime) {
		this.startTime = startTime;
	}
	public int getDocId() {
		return doc_id;
	}
	protected void setDocId(int doc_id) {
		this.doc_id = doc_id;
	}
	
	/**
	 * Returns the columns of the InformationBlock class responding to the provided
	 * table name "tableName"
	 * @param tableName
	 * @return the array of columns responding to the provided tableName
	 */
	public static String[] getColumns(String tableName) {
		try {
			if(tableName.equals(FactBlock.TABLE_NAME))
				return FactBlock.COLUMNS;
			if(tableName.equals(ImageBlock.TABLE_NAME))
				return ImageBlock.COLUMNS;
			if(tableName.equals(MapBlock.TABLE_NAME))
				return MapBlock.COLUMNS;
			if(tableName.equals(QuizBlock.TABLE_NAME))
				return QuizBlock.COLUMNS;
			throw new Exception("InformationBlock.class - method getColumns(String tableName " +
					"- Forgot to add new InformationBlock class)");
		} catch (Exception e) {
			Log.e(null, e.getMessage());
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * -----------------------------------------
	 * -----------------------------------------
	 * -- COMPARATOR
	 * -----------------------------------------
	 * -----------------------------------------
	 */
	
	/**
	 * Custom comparator
	 */
	public static final Comparator<InformationBlock> StartTimeComparator = new Comparator<InformationBlock>(){
		@Override
		public int compare(InformationBlock lhs, InformationBlock rhs) {
			return lhs.compareTo(rhs);
		}
    };
    
    /**
     * Comparison based on the start time of the information block
     * sorting based on start time ASC
     */
	@Override
	public int compareTo(InformationBlock another) {
		if(getStartTime() > another.getStartTime())
			return 1;
		if(getStartTime() < another.getStartTime())
			return -1;
		return 0;
	}
	
	
	
}
