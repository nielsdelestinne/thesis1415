package niels.master.thesis.prototypev2.entities;

public class MapBlock extends InformationBlock {

	public static final String TABLE_NAME = "map_block";
	public static final String[] COLUMNS = {"id", "doc_id", "title", "text", "start_time", 
		"map_url"};
	
	private String mapUrl;

	public MapBlock(int id, int docId, String title, String text, int startTime,
			String mapUrl) {
		setId(id);
		setDocId(docId);
		setType(TABLE_NAME);
		setTitle(title);
		setText(text);
		setStartTime(startTime);
		setMapUrl(mapUrl);
	}
	
	public String getMapUrl() {
		return mapUrl;
	}

	private void setMapUrl(String mapUrl) {
		this.mapUrl = mapUrl;
	}
	
}
