package niels.master.thesis.prototypev2.entities;

public class FactBlock extends InformationBlock {

	public static final String TABLE_NAME = "fact_block";
	public static final String[] COLUMNS = {"id", "doc_id", "title", "text", "start_time"};
	
	public FactBlock(int id, int docId, String title, String text, int startTime) {
		setId(id);
		setDocId(docId);
		setType(TABLE_NAME);
		setTitle(title);
		setText(text);
		setStartTime(startTime);
	}
}
