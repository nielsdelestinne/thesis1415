package niels.master.thesis.prototypev2.entities;

public class ImageBlock extends InformationBlock {

	public static final String TABLE_NAME = "image_block";
	public static final String[] COLUMNS = {"id", "doc_id", "title", "text", "start_time",
			"image1_url", "image2_url", "image3_url"};
	
	private String image1Url;
	private String image2Url;
	private String image3Url;
	
	public ImageBlock(int id, int docId, String title, String text, int startTime, 
			String image1Url, String image2Url, String image3Url) {
		setId(id);
		setDocId(docId);
		setType(TABLE_NAME);
		setTitle(title);
		setText(text);
		setStartTime(startTime);
		setImage1Url(image1Url);
		setImage2Url(image2Url);
		setImage3Url(image3Url);
	}
	
	public String getImage1Url() {
		return image1Url;
	}

	private void setImage1Url(String image1Url) {
		this.image1Url = image1Url;
	}

	public String getImage2Url() {
		return image2Url;
	}

	private void setImage2Url(String image2Url) {
		this.image2Url = image2Url;
	}

	public String getImage3Url() {
		return image3Url;
	}

	private void setImage3Url(String image3Url) {
		this.image3Url = image3Url;
	}
	
}
