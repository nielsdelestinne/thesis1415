package niels.master.thesis.prototypev2.entities;

public class QuizBlock extends InformationBlock {

	public static final String TABLE_NAME = "quiz_block";
	public static final String[] COLUMNS = {"id", "doc_id", "title", "text", "start_time",
			"choice1", "choice2", "choice3", "choice1_answer", "choice2_answer", "choice3_answer"};
	
	private String choice1;
	private String choice2;
	private String choice3;
	private String choice1Answer;
	private String choice2Answer;
	private String choice3Answer;
	
	public QuizBlock(int id, int docId, String title, String text, int startTime, 
			String choice1, String choice2, String choice3, String choice1Answer,
			String choice2Answer, String choice3Answer) {
		setId(id);
		setDocId(docId);
		setType(TABLE_NAME);
		setTitle(title);
		setText(text);
		setStartTime(startTime);
		setChoice1(choice1);
		setChoice2(choice2);
		setChoice3(choice3);
		setChoice1Answer(choice1Answer);
		setChoice2Answer(choice2Answer);
		setChoice3Answer(choice3Answer);
	}

	public String getChoice1() {
		return choice1;
	}

	private void setChoice1(String choice1) {
		this.choice1 = choice1;
	}

	public String getChoice2() {
		return choice2;
	}

	private void setChoice2(String choice2) {
		this.choice2 = choice2;
	}

	public String getChoice3() {
		return choice3;
	}

	private void setChoice3(String choice3) {
		this.choice3 = choice3;
	}

	public String getChoice1Answer() {
		return choice1Answer;
	}

	private void setChoice1Answer(String choice1Answer) {
		this.choice1Answer = choice1Answer;
	}

	public String getChoice2Answer() {
		return choice2Answer;
	}

	private void setChoice2Answer(String choice2Answer) {
		this.choice2Answer = choice2Answer;
	}

	public String getChoice3Answer() {
		return choice3Answer;
	}

	private void setChoice3Answer(String choice3Answer) {
		this.choice3Answer = choice3Answer;
	}
	
	
}
