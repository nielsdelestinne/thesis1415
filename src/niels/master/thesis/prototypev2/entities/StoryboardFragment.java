package niels.master.thesis.prototypev2.entities;

import java.io.Serializable;

public class StoryboardFragment  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8053371118243438735L;
	public static final String TABLE_NAME = "fragment";
	public static final String[] COLUMNS = {"id", "doc_id", "title", "description", "image", "start_time", "duration", "information_id", "information_type"};
	
	private int id;
	private int doc_id;
	private String title;
	private String description;
	private String image;
	private int start_time;
	private int duration;
	private int informationId;
	private String informationType;
	
	public StoryboardFragment(int id, int doc_id, String title, String description, String image, int start_time, int duration, int informationId, String informationType) {
		setId(id);
		setDoc_id(doc_id);
		setTitle(title);
		setDescription(description);
		setImage(image);
		setStart_time(start_time);
		setDuration(duration);
		setInformationId(informationId);
		setInformationType(informationType);
	}

	public int getId() {
		return id;
	}

	private void setId(int id) {
		this.id = id;
	}

	public int getDoc_id() {
		return doc_id;
	}

	private void setDoc_id(int doc_id) {
		this.doc_id = doc_id;
	}

	public String getTitle() {
		return title;
	}

	private void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	private void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	private void setImage(String image) {
		this.image = image;
	}

	public int getStart_time() {
		return start_time;
	}

	private void setStart_time(int start_time) {
		this.start_time = start_time;
	}

	public int getDuration() {
		return duration;
	}

	private void setDuration(int duration) {
		this.duration = duration;
	}
	
	public String toString() {
		return "StoryboardFragment = " + getId();
	}

	public int getInformationId() {
		return informationId;
	}

	private void setInformationId(int informationId) {
		this.informationId = informationId;
	}

	public String getInformationType() {
		return informationType;
	}

	private void setInformationType(String informationType) {
		this.informationType = informationType;
	}
}
