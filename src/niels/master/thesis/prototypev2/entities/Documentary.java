package niels.master.thesis.prototypev2.entities;

public class Documentary {

	public static final String TABLE_NAME = "documentary";
	public static final String[] COLUMNS = {"id", "name", "description", "duration", "image_url"};
	public static final int CURRENT_DOC_ID = 1;
	
	private int id;
	private String name;
	private String description;
	private int duration;
	private String imageUrl;
	
	/**
	 * Uses the values in cursor as 
	 * parameters for the setters
	 * @param cursor
	 */
	public Documentary(int id, String name, String description, int duration, String imageUrl) {
		setId(id);
		setName(name);
		setDescription(description);
		setDuration(duration);
		setImageUrl(imageUrl);
	}
	
	public int getId() {
		return id;
	}

	private void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	private void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	private void setDescription(String description) {
		this.description = description;
	}

	public int getDuration() {
		return duration;
	}

	private void setDuration(int duration) {
		this.duration = duration;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	private void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
	
	
}
