package niels.master.thesis.prototypev2.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.annotation.SuppressLint;
import android.content.Context;

public class DatabaseBootstrapper {

	public final static String DATABASE_NAME = "prototypev2.sqlite";
	private String destDir;
	private String destPath;
	private Context context;

	@SuppressLint("SdCardPath") 
	public DatabaseBootstrapper(Context context, String packageName) {
		// Destination of database on Android device
		//destDir = context.getFilesDir().getPath() + packageName + "/databases/";
		destDir = "/data/data/"+ packageName + "/databases/";
		destPath = destDir + DATABASE_NAME;
		// Set the context
		this.context = context;
	}

	/**
	 * Stores the SQLite database on the Android device if it is not yet
	 * present. Does nothing when it is present.
	 */
	public void storeDatabaseOnDevice() {
		File f = new File(destPath);
		if (!f.exists()) {
			System.out.println("Database did not exist: " + destPath);
			// ---make sure directory exists---
			File directory = new File(destDir);
			directory.mkdirs();
			// ---copy the db from the assets folder into
			// the databases folder---
			try {
				CopyDB(context.getAssets().open(DATABASE_NAME),
						new FileOutputStream(destPath));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void CopyDB(InputStream inputStream, OutputStream outputStream)
			throws IOException {
		// ---copy 1K bytes at a time---
		byte[] buffer = new byte[1024];
		int length;
		while ((length = inputStream.read(buffer)) > 0) {
			outputStream.write(buffer, 0, length);
		}
		inputStream.close();
		outputStream.close();
	}

}
