package niels.master.thesis.prototypev2.database;

import java.util.ArrayList;
import java.util.Collections;

import niels.master.thesis.prototypev2.entities.Documentary;
import niels.master.thesis.prototypev2.entities.FactBlock;
import niels.master.thesis.prototypev2.entities.ImageBlock;
import niels.master.thesis.prototypev2.entities.InformationBlock;
import niels.master.thesis.prototypev2.entities.MapBlock;
import niels.master.thesis.prototypev2.entities.QuizBlock;
import niels.master.thesis.prototypev2.entities.StoryboardFragment;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseAdapter {
	static final String TAG = "DBAdapter";
	static final String DATABASE_NAME = DatabaseBootstrapper.DATABASE_NAME;
	static final int DATABASE_VERSION = 1;
	
	static final String DATABASE_CREATE = "create table contacts (_id integer primary key autoincrement, "
			+ "name text not null, email text not null);";
	final Context context;
	DatabaseHelper DBHelper;
	SQLiteDatabase db;

	public DatabaseAdapter(Context ctx) {
		this.context = ctx;
		DBHelper = new DatabaseHelper(context);
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			try {
				db.execSQL(DATABASE_CREATE);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS contacts");
			onCreate(db);
		}
	}

	// ---opens the database---
	public DatabaseAdapter open() throws SQLException {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	// ---closes the database---
	public void close() {
		DBHelper.close();
	}
	
//	// ---insert a contact into the database---
//	public long insertContact(String name, String email) {
//		ContentValues initialValues = new ContentValues();
//		initialValues.put(KEY_NAME, name);
//		initialValues.put(KEY_EMAIL, email);
//		return db.insert(DATABASE_TABLE, null, initialValues);
//	}
//
//	// ---deletes a particular contact---
//	public boolean deleteContact(long rowId) {
//		return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
//	}
//
//	// ---retrieves all the contacts---
//	public Cursor getAllContacts() {
//		return db.query(DATABASE_TABLE, new String[] { KEY_ROWID, KEY_NAME,
//				KEY_EMAIL }, null, null, null, null, null);
//	}

	public ArrayList<InformationBlock> getInformationBlocksForSpecifiedDocumentary(int docId) throws Exception {
		ArrayList<InformationBlock> informationBlocks = new ArrayList<InformationBlock>();
		// FOREACH TABLE IN TABLES
		for (String tableName : InformationBlock.TABLES) {
			// GET ALL THE INFORMATIONBLOCKS, FROM THE TABLE IDENTIFIED BY tableName, 
			// THAT ARE LINKED TO THE DOCUMENTARY IDENTIFIED WITH docId.
			informationBlocks.addAll(
					getListOfInformationBlocksByDocId(docId, tableName, InformationBlock.getColumns(tableName)));
		}
		// SORT THE COMPLETE LIST BEFORE RETURNING
		Collections.sort(informationBlocks, InformationBlock.StartTimeComparator);
		return informationBlocks;
	}
	
	/**
	 * Returns the Documentary object, identified by id,
	 * which is stored in the the table identified by tableName. 
	 * @param id
	 * @param tableName
	 * @param columns
	 * @return Documentary
	 */
	public Documentary getSingleDocumentaryById(int id, String tableName, String[] columns) {
		Cursor cursor = dbQueryCall("id", id, tableName, columns);
		return new Documentary(cursor.getInt(0), cursor.getString(1), 
				cursor.getString(2), cursor.getInt(3), cursor.getString(4));
	}
	
	/**
	 * Returns the Documentaries that are watched before.
	 * @param tableName
	 * @param columns
	 * @return ArrayList<Documentary>
	 */
	public ArrayList<Documentary> getWatchedBeforeDocumentaries(String tableName, String[] columns) {
		ArrayList<Documentary> documentaries = new ArrayList<Documentary>();
		Cursor cursor = dbQueryCall("watched_before", 1, tableName, columns);
		while(!cursor.isAfterLast()) {
			documentaries.add(new Documentary(cursor.getInt(0), cursor.getString(1), 
					cursor.getString(2), cursor.getInt(3), cursor.getString(4)));
			cursor.moveToNext();
		}
		return  documentaries;
	}
	
	/**
	 * Returns the InformationBlock object, identified by id,
	 * which is stored in the the table identified by tableName. 
	 * @param id
	 * @param tableName
	 * @param columns
	 * @return InformationBlock
	 */
	public InformationBlock getSingleInformationBlockById(int id, String tableName, String[] columns) {
		Cursor mCursor = dbQueryCall("id", id, tableName, columns);
		InformationBlock informationBlock = null;
		try {
			informationBlock = createInformationBlockObject(mCursor, tableName);
		} catch (Exception e) {
			Log.e(null, e.getMessage());
			e.printStackTrace();
		}
		return informationBlock;
	}
	
	/**
	 * Returns the list of InformationBlock objects that are stored
	 * in the table identified by tableName and linked to the documentary identified by docId.
	 * @param docId = the id of the documentary
	 * @param tableName
	 * @param columns
	 * @return ArrayList<InformationBlock>
	 */
	public ArrayList<InformationBlock> getListOfInformationBlocksByDocId(int docId, String tableName, String[] columns) {
		Cursor mCursor = dbQueryCall("doc_id", docId, tableName, columns);
		ArrayList<InformationBlock> informationBlockList = null;
		try {
			informationBlockList = createInformationBlockObjects(mCursor, tableName);
		} catch (Exception e) {
			Log.e(null, e.getMessage());
			e.printStackTrace();
		}
		return informationBlockList;
	}
	
	/**
	 * Helper function that does a query to the database.
	 * One filter option and filter value can be specified. 
	 * e.g. ("WHERE ID = 1") -> ID = filter option, 1 = filter value
	 * @param filter
	 * @param filterValue
	 * @param tableName
	 * @param columns
	 * @return Cursor containing the results of the query
	 */
	private Cursor dbQueryCall(String filter, int filterValue, String tableName, String[] columns) {
		Cursor mCursor = db.query(true, tableName, columns, filter + "=" + filterValue,
				null, null, null, null, null);
		if (mCursor != null) {
			mCursor.moveToFirst();
		}
		return mCursor;
	}
	
	/**
	 * Method that loops through the cursor and creates an
	 * InformationBlock object for every position of the cursor.
	 * TL;DR: transforms the Cursor object to an ArrayList<InformationBlock> object.
	 * @param cursor
	 * @param tableName
	 * @return ArrayList containing InformationBlock objects
	 * @throws Exception 
	 */
	private ArrayList<InformationBlock> createInformationBlockObjects(Cursor cursor, String tableName) throws Exception {
		ArrayList<InformationBlock> list = new ArrayList<InformationBlock>();
		while(!cursor.isAfterLast()) {
			list.add(createInformationBlockObject(cursor, tableName));
			cursor.moveToNext();
		}
		Collections.sort(list, InformationBlock.StartTimeComparator);
		return list;
	}
	
	/**
	 * Method that uses Cursor objects with only one position.
	 * Transforms it to an InformationBlock object.
	 * @param cursor
	 * @param tableName
	 * @return InformationBlock object
	 * @throws Exception 
	 */
	private InformationBlock createInformationBlockObject(Cursor cursor, String tableName) throws Exception {
		if(tableName.equals(FactBlock.TABLE_NAME))
			return new FactBlock(cursor.getInt(0), cursor.getInt(1), 
					cursor.getString(2), cursor.getString(3), 
					cursor.getInt(4));
		if(tableName.equals(ImageBlock.TABLE_NAME))
			return new ImageBlock(cursor.getInt(0), cursor.getInt(1), 
					cursor.getString(2), cursor.getString(3), 
					cursor.getInt(4), cursor.getString(5), 
					cursor.getString(6), cursor.getString(7));
		if(tableName.equals(MapBlock.TABLE_NAME))
			return new MapBlock(cursor.getInt(0), cursor.getInt(1), 
					cursor.getString(2), cursor.getString(3), 
					cursor.getInt(4), cursor.getString(5));
		if(tableName.equals(QuizBlock.TABLE_NAME))
			return new QuizBlock(cursor.getInt(0), cursor.getInt(1), 
					cursor.getString(2), cursor.getString(3), 
					cursor.getInt(4), cursor.getString(5), cursor.getString(6),
					cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10));
		throw new Exception("DatabaseAdapter.class - method createInformationBlockObject - ) " +
					"forgot to add new InformationBlock to switch");
	}
	
	/**
	 * Method that collects the storyboard fragments for a certain documentary
	 * @param docId
	 * @return List containing storyboard fragments.
	 */
	public ArrayList<StoryboardFragment> getStoryboardFragmentsForSpecifiedDocumentary(int docId){
		Cursor mCursor = dbQueryCall("doc_id", docId, StoryboardFragment.TABLE_NAME, StoryboardFragment.COLUMNS);
		ArrayList<StoryboardFragment> storyboardFragments = new ArrayList<StoryboardFragment>();
		while(!mCursor.isAfterLast()) {
			storyboardFragments.add(new StoryboardFragment(mCursor.getInt(0), mCursor.getInt(1), 
					mCursor.getString(2), mCursor.getString(3), mCursor.getString(4), 
					mCursor.getInt(5), mCursor.getInt(6), mCursor.getInt(7), mCursor.getString(8)));	
			mCursor.moveToNext();
		}
		return storyboardFragments;
	}
	
//	// ---updates a contact---
//	public boolean updateContact(long rowId, String name, String email) {
//		ContentValues args = new ContentValues();
//		args.put(KEY_NAME, name);
//		args.put(KEY_EMAIL, email);
//		return db.update(DATABASE_TABLE, args, KEY_ROWID + "=" + rowId, null) > 0;
//	}
	
	
	

}
