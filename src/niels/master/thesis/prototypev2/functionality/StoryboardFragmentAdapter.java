package niels.master.thesis.prototypev2.functionality;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.jess.ui.TwoWayGridView;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.activities.func2.BtnClickListener;
import niels.master.thesis.prototypev2.entities.StoryboardFragment;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("UseSparseArrays")
public class StoryboardFragmentAdapter extends BaseAdapter{

	private Context context;
	private StoryboardFragment[] storyboardFragments;
	private BtnClickListener showClickListener = null;
	private HashMap<Integer, Integer> clickedOnItems = new HashMap<Integer, Integer>();
	private TwoWayGridView parentGridView;
	private Button btnStart;
	private int cursor = 1;
	
	public StoryboardFragmentAdapter(Context context, Button btnStart, StoryboardFragment[] storyboardFragments, BtnClickListener showListener, TwoWayGridView gridView) {
		this.context = context;
		this.btnStart = btnStart;
		this.storyboardFragments = storyboardFragments;
		this.showClickListener = showListener;
		this.parentGridView = gridView;
	}
	
	@Override
	public int getCount() {
		return storyboardFragments.length;
	}

	@Override
	public Object getItem(int position) {
		return storyboardFragments[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return storyboardFragments[position].getId();
	}
	
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) {
			// get layout from mobile.xml
			convertView = inflater.inflate(R.layout.grid_storyboardfragment, null);
		}
		
		// set value into textview
		TextView textView = (TextView) convertView
				.findViewById(R.id.grid_item_label);
		textView.setText(storyboardFragments[position].getTitle());
		// set image into imageview
		// set image based on selected text
		ImageView imageView = (ImageView) convertView
				.findViewById(R.id.grid_item_image);
		setImageForImageView(imageView, storyboardFragments[position].getImage());
		imageView.setOnClickListener(createShowInformationOnClickListener(storyboardFragments[position]));
 
		// Show info button
		Button btnShowInfo = (Button) convertView.findViewById(R.id.grid_button);
		btnShowInfo.setTag(storyboardFragments[position].getId()); //For passing the list item index
		btnShowInfo.setOnClickListener(createShowInformationOnClickListener(storyboardFragments[position]));	
		
		// Add fragment button
		Button btnAdd = (Button) convertView.findViewById(R.id.grid_add_button);
		btnAdd.setTag(storyboardFragments[position].getId()); //For passing the list item index
		btnAdd.setOnClickListener(createAddOnClickListener());	
		
		//System.out.println("Position: " + position + ", id = " + storyboardFragments[position].getId() + ", added?=" + clickedOnItems.containsKey(storyboardFragments[position].getId()));

		/*
		 * clickedOnItems contains the the id's of the storyboard fragments who have 
		 * been clicked on. For every id (key) in ClickedOnItems, we have an ordering int (Value).
		 * 1 or more means the fragment is added to the storyboard. -1 means it was removed from the
		 * storyboard after being added. If the id is not yet in ClickedOnItems, the fragment is not
		 * clicked on before.
		 */
		if(clickedOnItems.containsKey(storyboardFragments[position].getId())){
			if(clickedOnItems.get(storyboardFragments[position].getId()) != -1) {
				// Fragment added to the storyboard
		    	btnAdd.setBackgroundColor(context.getResources().getColor(R.color.red));
		    	btnAdd.setText(clickedOnItems.get(storyboardFragments[position].getId())+ "");
	    	}
	    	else {
	    		// Fragment is removed from the storyboard
	    		btnAdd.setBackgroundColor(context.getResources().getColor(R.color.green));
		    	btnAdd.setText(context.getResources().getString(R.string.btn_add));
		    	clickedOnItems.put(storyboardFragments[position].getId(), -1);
	    	}
		}
		else {
			// Fragment is not yet added / clicked on
			btnAdd.setBackgroundColor(context.getResources().getColor(R.color.green));
	    	btnAdd.setText(context.getResources().getString(R.string.btn_add));
		}
		
		return convertView;
	}
	
	/**
	 * Executed when the "Show Information" button is clicked
	 */
	private OnClickListener oCLShow = null;
	private OnClickListener createShowInformationOnClickListener(final StoryboardFragment storyboardFragment) {
		oCLShow = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showClickListener.onBtnClick(storyboardFragment, v);	
			}
		};
		return oCLShow;
	}
	

	/**
	 * Executed when the "Add" button is clicked
	 */
	private OnClickListener oCLAdd = null;
	private OnClickListener createAddOnClickListener() {
		if(oCLAdd == null) {
			oCLAdd = new View.OnClickListener() {
			    @Override
			    public void onClick(View v) {
			    	
		        	//addClickListener.onBtnClick(position, v);
		        	if(!clickedOnItems.containsKey((Integer) v.getTag())) {
		        		clickedOnItems.put((Integer) v.getTag(), cursor);
		        		cursor++;
		        		Toast.makeText(context, "Fragment added to storyboard", Toast.LENGTH_SHORT).show();
		        		btnStart.setText(R.string.btn_startStoryboard_selected);
		        	}
		        	else {
		        		int order = clickedOnItems.get((Integer) v.getTag());
		        		clickedOnItems.remove((Integer) v.getTag());
		        		cursor--;
		        		Toast.makeText(context, "Fragment removed storyboard", Toast.LENGTH_SHORT).show();
				    	// Refresh cursor
		        		for (Integer key : clickedOnItems.keySet()) {
							if(clickedOnItems.get(key) > order && order != -1) {
								clickedOnItems.put(key, clickedOnItems.get(key) - 1);
							}
						}
		        		if(clickedOnItems.size() == 0)
		        			btnStart.setText(R.string.btn_startStoryboard);
		        	}

			        // Make sure that all the child views of the parent gridview are
			        // redrawn
			        parentGridView.invalidateViews();
			    }
			};
		}
		return oCLAdd;
	}
	
	/**
	 * Method that is called when the user finishes selected his
	 * fragments and wants to play the selected fragments.
	 * @return an arraylist containing the fragments in the order
	 * 			as created by the user.
	 */
	public ArrayList<StoryboardFragment> getSelectedFragments() {
		Map<Integer, Integer> fragmentsMap = new LinkedHashMap<Integer, Integer>();
		// copy map
		for (Integer key : clickedOnItems.keySet()) {
			if(clickedOnItems.get(key) != -1)
				fragmentsMap.put(key, clickedOnItems.get(key));
		}
		// sort the map, returning an arraylist containing the storyboard fragments
		// in their selected order.
		return sortMapContainingFragments(fragmentsMap);
	}

	
	/**
	 * sort the map, returning an arraylist containing the storyboard fragments
	 * in their selected order.
	 * @return sorted arraylist
	 */
	private ArrayList<StoryboardFragment> sortMapContainingFragments(Map<Integer, Integer> fragmentsMap) {
		ArrayList<StoryboardFragment> fragments = new ArrayList<StoryboardFragment>();
		// sort and transform map
		List<Map.Entry<Integer, Integer>> entries =
		  new ArrayList<Map.Entry<Integer, Integer>>(fragmentsMap.entrySet());
		Collections.sort(entries, new Comparator<Map.Entry<Integer, Integer>>() {
		  public int compare(Map.Entry<Integer, Integer> a, Map.Entry<Integer, Integer> b){
		    return a.getValue().compareTo(b.getValue());
		  }
		});
		Map<Integer, Integer> sortedMap = new LinkedHashMap<Integer, Integer>();
		for (Map.Entry<Integer, Integer> entry : entries) {
		  sortedMap.put(entry.getKey(), entry.getValue());
		  fragments.add(storyboardFragments[entry.getKey()-1]);
		}
		return fragments;
	}
	
	/**
	 * Helper method for loading in an image from
	 * the assets folder and setting it as the src
	 * for the provided ImageView object.
	 * @param imageView
	 * @param imageUrl
	 */
	private void setImageForImageView(ImageView imageView, String imageUrl) {
		imageUrl = imageUrl.substring(0, imageUrl.lastIndexOf('.'));
		int resID = context.getResources().getIdentifier(imageUrl, "drawable",  context.getPackageName());
		imageView.setImageResource(resID);
	}

}
