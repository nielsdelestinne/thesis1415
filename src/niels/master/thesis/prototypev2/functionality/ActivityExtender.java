package niels.master.thesis.prototypev2.functionality;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;

public class ActivityExtender extends Activity {
	
	/**
	 * Method that removes the upper bar that holds the time, battery information, etc... 
	 * A.K.A as "full screen mode"
	 */
	public void fullScreenMode() {
		// no action bar
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
    		 WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
}
