package niels.master.thesis.prototypev2.functionality;

import java.util.ArrayList;
import java.util.Collections;

import niels.master.thesis.prototypev2.activities.func1.HomeScreenActivity;
import niels.master.thesis.prototypev2.activities.func1.InformationBlockActivity;
import niels.master.thesis.prototypev2.activities.func1.NextUpActivity;
import niels.master.thesis.prototypev2.database.DatabaseAdapter;
import niels.master.thesis.prototypev2.entities.Documentary;
import niels.master.thesis.prototypev2.entities.FactBlock;
import niels.master.thesis.prototypev2.entities.ImageBlock;
import niels.master.thesis.prototypev2.entities.InformationBlock;
import niels.master.thesis.prototypev2.entities.MapBlock;
import android.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

public class SynchronizationHandler {

	public static final String PASSED_MESSAGE = "PASSED_MESSAGE";
	
	private static SynchronizationHandler instance = null;
	public static final int INTERVAL = 1000; // milliseconds
	private static final int DURATION_OF_BLOCK = 10; // seconds
	private int currentTime = 0;
	private int endTime;
	private boolean justStarted = true;
	private boolean nextUpComing = false;
	private int nextUpCounter = 0;
	private ArrayList<InformationBlock> informationBlockList = new ArrayList<InformationBlock>();
	private ArrayList<InformationBlock> shownInformationBlockList = new ArrayList<InformationBlock>();
	private int cursor = -1;
	private int initialAmountOfInformationBlockObjects = 0;
	private boolean isThreadStopped = false;
	
	
	protected SynchronizationHandler(int endTime){
		setEndTime(endTime);
		informationBlockList = loadInformationBlocks();
		initialAmountOfInformationBlockObjects = informationBlockList.size();
		start();
	}
	
	public static SynchronizationHandler getInstance(int endTime){
		if(instance == null){
			instance = new SynchronizationHandler(endTime);
		}
		return instance;
	}
	
	public void reset() {
		isThreadStopped = true;
		instance = null;
	}

	private void setEndTime(int endTime) {
		this.endTime = endTime;
	}

	private void start() {
		final Handler mHandler = new Handler();
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	            // TODO Auto-generated method stub
	        	
	            while (currentTime < endTime && !isThreadStopped) {
	                try {
	                    Thread.sleep(INTERVAL);

	                    mHandler.post(new Runnable() {
	                        @Override
	                        public void run() {
	                        	debug();
	                        	manageTheSynchronization();
	                        }
	                    });
	                } catch (Exception e) {
	                    // TODO: handle exception
	                }
	            }
	        }
		}).start();
	}
	
	private void debug() {
		System.out.println(currentTime + "  " + endTime);
	}
	
	private void manageTheSynchronization() {
		// IF THERE ARE STILL INFORMATION BLOCKS TO SHOW
		if(!informationBlockList.isEmpty()){			
			// IF IT IS TIME TO SHOW AN INFORMATION BLOCK
			if(currentTime == informationBlockList.get(0).getStartTime()) {
				launchInformationBlock();
			}
			// IF A "NEXT UP" SCREEN SHOULD BE AUTOMATICALLY SHOWN
			if(nextUpComing) {
				// launchNextUpBlockIntent(nextUpBlockIntent());
			}
			// WHEN WE START, THE FIRST SCREEN SHOULD BE A
			// "NEXT UP" SCREEN
			if(currentTime == 0) {
				Log.d(null, "First Next Up screen should be shown");
				launchNextUpBlockIntent(nextUpBlockIntent());
				justStarted = false;
			}
		}
		currentTime++;
	}

	/**
	 * Wrapper method that creates a new intent
	 * and launches a new activity with the correct parameters.
	 */
	private void launchInformationBlock() {
		Log.i(null, "New information Block will be launched!");
		// store the shown information block in the list for 
		// shown InformationBlock objects
		storeShownInformationBlock(informationBlockList.get(0));
		// Create new intent
		Intent intent = createInformationBlockIntent(informationBlockList.get(0).getId(), 
				informationBlockList.get(0).getType(), 1, true);
		// remove the shown information block from the list of 
		// InformationBlock objects to be shown
		informationBlockList.remove(0);
		// BY SETTING THIS VALUE TO TRUE, A NEXTUP ACTIVITY WILL
		// BE SHOWN AFTER DURATION_OF_BLOCK SECONDS.
		nextUpComing = true;
		// SET THE CONTEXT
		Context context = selectContext(InformationBlockActivity.CONTEXT);
		// LAUNCH THE INTENT
		context.startActivity(intent);
		
	}
	
	public Intent launchPreviousInformationBlock(boolean sourceNextUp) {
		// IF THE CURSOR > 0 OR IF THE CURSOR == 0 AND THE PREVIOUS BUTTON
		// WAS PRESSED FROM INSIDE A NEXTUP ACTIVITY (needed so we can go back to
		// the first InformationBlock activity when the 2nd nextup activity is being shown)
		if(cursor > 0 || (cursor == 0 && sourceNextUp)) {
			Log.i(null, "Previous information Block will be launched!");
			int hasNextUpButton = 1;
			// BY SETTING THIS VALUE TO FALSE, NO NEXTUP BLOCK 
			// WILL BE SHOWN AUTOMATICALLY AFTER DURATION_OF_BLOCK SECONDS
			nextUpComing = false;
			// ONLY IF THE PREVIOUS BUTTON WAS PRESSED FROM INSIDE AN INFORMATIONBLOCK
			// ACTIVITY DO WE REDUCE THE CURSOR. NOT WHEN PRESSED FROM INSDE A NEXTUP
			// ACTIVITY
			if(!sourceNextUp) {
				hasNextUpButton = 0;
				// SUBTRACT 1 FROM THE CURSOR
				cursor--;			
			}
			return createInformationBlockIntent(shownInformationBlockList.get(cursor).getId(), 
					shownInformationBlockList.get(cursor).getType(), hasNextUpButton, true);			
		}
		return null;
	}
	
	public Intent launchNextInformationBlock() {
		if(cursor < shownInformationBlockList.size() - 1) {
			Log.i(null, "Next information Block will be launched!");
			// ADD 1 TO THE CURSOR
			cursor++;
			// DETERMINE IF THERE SHOULD BE A "SHOW WHAT IS NEXT UP" BUTTON
			int hasNextUpButton = 0;
			if(cursor == shownInformationBlockList.size() - 1)
				hasNextUpButton = 1;
			return createInformationBlockIntent(shownInformationBlockList.get(cursor).getId(), 
					shownInformationBlockList.get(cursor).getType(), hasNextUpButton, false);
		}
		// WE HAVE NO NEXT INFORMATIONBLOCK ACTIVITY TO SHOW
		// SO WE SHOW THE NEXTUP ACTIVITY (IF THERE IS ANY)
		else {
			// IF THERE IS A NEXTUP ACTIVITY TO SHOW
			if(!informationBlockList.isEmpty()) {
				nextUpCounter = DURATION_OF_BLOCK;
				return nextUpBlockIntent();
			}
			return null;
		}
	}
	
	/**
	 * Creates and returns an Intent for an 
	 * InformationBlockActivity object
	 * @param id
	 * @param type
	 * @param hasNextUpButton
	 * @param fromLeft
	 * @return Intent
	 */
	private Intent createInformationBlockIntent(int id, String type, int hasNextUpButton, boolean fromLeft) {
		// SET THE CONTEXT
		Context context = selectContext(InformationBlockActivity.CONTEXT);
		// LAST INFORMATION BLOCK DOES NOT NEED A NEXT (UP) BUTTON WHATSOEVER
		if(shownInformationBlockList.size() == initialAmountOfInformationBlockObjects 
				&& cursor == shownInformationBlockList.size() - 1)
			hasNextUpButton = -1;
		// IF ONLY THE FIRST INFORMATION BLOCK IS LAUNCHED, WE DO NOT NEED A PREVIOUS BUTTON
		int hasPreviousButton = 1;
		if(cursor == 0)
			hasPreviousButton = -1;
		Intent intent = new Intent(context, InformationBlockActivity.class);
		String[] message = {id + "", type, hasNextUpButton + "", hasPreviousButton + ""};
		intent.putExtra(PASSED_MESSAGE, message);
		return intent;
	}
	
	/**
	 * Wrapper method that creates an Intent for NextUpActivity.
	 * Only returns a valid intent if provided parameters satisfy
	 * the conditions.
	 * @return Intent for launching a NextUpActivity
	 */
	private Intent nextUpBlockIntent() {
		// WE WAIT UNTIL THE PREVIOUS INFORMATION BLOCK IS SHOWN FOR THE TIME
		// EQUAL TO DURATION_OF_BLOCK BEFORE SHOWING THE "NEXT UP" SCREEN
		if(nextUpCounter == DURATION_OF_BLOCK || justStarted) {
			Log.i(null, "New Next Up screen will be launched!");
			Intent intent = createNextUpIntent(informationBlockList.get(0).getTitle(), 
					informationBlockList.get(0).getStartTime(), currentTime);
			// no need to show another next up screen after the current one
			nextUpComing = false;
			// restart the counter
			nextUpCounter = 0;
			// re-initialize cursor
			cursor = shownInformationBlockList.size() - 1;
			return intent;
		}
		else {
			nextUpCounter++;
			return null;
		}
	}
	
	/**
	 * method that creates and returns an Intent 
	 * for NextUpActivity.
	 * @param title
	 * @param startTime
	 * @param currentTime
	 * @return Intent for launching a NextUpActivity
	 */
	private Intent createNextUpIntent(String title, int startTime, int currentTime) {
		// SET THE CONTEXT
		Context context = selectContext(NextUpActivity.CONTEXT);
		// IF THE FIRST NEXT UP ACTIVITY IS BEING SHOWN, 
		// WE DO NOT NEED A PREVIOUS BUTTON
		int hasPreviousButton = 1;
		if(shownInformationBlockList.size() == 0)
			hasPreviousButton = -1;
		Intent intent = new Intent(context, NextUpActivity.class);
		String[] message = {title, startTime + "", currentTime + "", hasPreviousButton + ""};
		intent.putExtra(PASSED_MESSAGE, message);
		return intent;
	}
	
	/**
	 * Helper method for checking if the provided Intent object
	 * is not null and if not, to launch a new NextUp Activity
	 * using the provided Intent object.
	 * @param nextUpBlockIntent
	 */
	private void launchNextUpBlockIntent(Intent nextUpBlockIntent) {
		if(nextUpBlockIntent != null) {
			// SET THE CONTEXT
			Context context = selectContext(NextUpActivity.CONTEXT);
			// LAUNCH THE INTENT
			context.startActivity(nextUpBlockIntent);
		}
	}
	
	/**
	 * Method for adding the last shown InformationBlock object
	 * to a list and resetting the cursor of the list to the
	 * last position.
	 * @param informationBlock
	 */
	private void storeShownInformationBlock(InformationBlock informationBlock) {
		shownInformationBlockList.add(informationBlock);
		cursor = shownInformationBlockList.size() - 1;
	}
	
	/**
	 * Loads the information blocks from the database and places it in the memory.
	 * InformationBlock objects are created
	 */
	private ArrayList<InformationBlock> loadInformationBlocks() {
		DatabaseAdapter dba = new DatabaseAdapter(HomeScreenActivity.CONTEXT);
		dba.open();
		ArrayList<InformationBlock> informationBlocks = null;
		try {
			informationBlocks = dba.getInformationBlocksForSpecifiedDocumentary(Documentary.CURRENT_DOC_ID);
		} catch (Exception e) {
			Log.e(null, e.getMessage());
			e.printStackTrace();
		}
		dba.close();
		return informationBlocks;
	}
	
//	private void startTransitionAnimation(Context context, boolean fromLeft) {
//		// SET THE TRANSITION ANIMATION
//		Activity act = (Activity) context;
//		if (fromLeft)
//			act.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
//		else act.overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_left);
//	}

	/**
	 * Method that checks if the preferredContext can be used
	 * @param preferredContext
	 * @return a valid context
	 */
	private Context selectContext(Context preferredContext) {
		if(preferredContext == null) 
			return HomeScreenActivity.CONTEXT;
		return preferredContext;
	}
	
}
