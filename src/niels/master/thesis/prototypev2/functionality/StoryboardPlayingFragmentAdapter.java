package niels.master.thesis.prototypev2.functionality;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.jess.ui.TwoWayGridView;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.activities.func2.BtnClickListener;
import niels.master.thesis.prototypev2.entities.StoryboardFragment;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("UseSparseArrays")
public class StoryboardPlayingFragmentAdapter extends BaseAdapter{

	private Context context;
	private StoryboardFragment[] storyboardFragments;
	private BtnClickListener showClickListener = null;
	private HashMap<Integer, Integer> clickedOnItems = new HashMap<Integer, Integer>();
	private TwoWayGridView parentGridView;
	private int cursor = 1;
	
	public StoryboardPlayingFragmentAdapter(Context context, StoryboardFragment[] storyboardFragments, BtnClickListener showListener, TwoWayGridView gridView) {
		this.context = context;
		this.storyboardFragments = storyboardFragments;
		this.showClickListener = showListener;
		this.parentGridView = gridView;
	}
	
	@Override
	public int getCount() {
		return storyboardFragments.length;
	}

	@Override
	public Object getItem(int position) {
		return storyboardFragments[position];
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return storyboardFragments[position].getId();
	}
	
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) {
			// get layout from mobile.xml
			convertView = inflater.inflate(R.layout.grid_storyboardplayingfragment, null);
		}
		
		// set value into textview
		TextView textView = (TextView) convertView
				.findViewById(R.id.grid_item_label);
		textView.setText(storyboardFragments[position].getTitle());
		// set image into imageview
		// set image based on selected text
		ImageView imageView = (ImageView) convertView
				.findViewById(R.id.grid_item_image);
		setImageForImageView(imageView, storyboardFragments[position].getImage());
		imageView.setOnClickListener(createShowInformationOnClickListener(storyboardFragments[position]));
 
		// Show info button
		Button btnShowInfo = (Button) convertView.findViewById(R.id.grid_button);
		btnShowInfo.setTag(storyboardFragments[position].getId()); //For passing the list item index
		btnShowInfo.setOnClickListener(createShowInformationOnClickListener(storyboardFragments[position]));	
		
		return convertView;
	}
	
	/**
	 * Executed when the "Show Information" button is clicked
	 */
	private OnClickListener oCLShow = null;
	private OnClickListener createShowInformationOnClickListener(final StoryboardFragment storyboardFragment) {
		oCLShow = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showClickListener.onBtnClick(storyboardFragment, v);	
			}
		};
		
		//parentGridView.invalidateViews();
		
		return oCLShow;
	}

	/**
	 * Helper method for loading in an image from
	 * the assets folder and setting it as the src
	 * for the provided ImageView object.
	 * @param imageView
	 * @param imageUrl
	 */
	private void setImageForImageView(ImageView imageView, String imageUrl) {
		imageUrl = imageUrl.substring(0, imageUrl.lastIndexOf('.'));
		int resID = context.getResources().getIdentifier(imageUrl, "drawable",  context.getPackageName());
		imageView.setImageResource(resID);
	}

}
