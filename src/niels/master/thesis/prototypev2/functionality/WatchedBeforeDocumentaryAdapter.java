package niels.master.thesis.prototypev2.functionality;

import java.util.ArrayList;
import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.entities.Documentary;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class WatchedBeforeDocumentaryAdapter extends BaseAdapter{

	private Context context;
	private Documentary[] documentaries;
	
	public WatchedBeforeDocumentaryAdapter(Context context, Documentary[] documentaries) {
		this.context = context;
		this.documentaries = documentaries;
	}
	
	@Override
	public int getCount() {
		return documentaries.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.grid_watched_before, null);
			}

			// set value into textview
			TextView textView = (TextView) convertView
					.findViewById(R.id.grid_item_label);
			textView.setText(documentaries[position].getName());
			// set image into imageview
			// set image based on selected text
			ImageView imageView = (ImageView) convertView
					.findViewById(R.id.grid_item_image);
			setImageForImageView(imageView, documentaries[position].getImageUrl());
	 
			return convertView;
	}
	
	/**
	 * Helper method for loading in an image from
	 * the assets folder and setting it as the src
	 * for the provided ImageView object.
	 * @param imageView
	 * @param imageUrl
	 */
	private void setImageForImageView(ImageView imageView, String imageUrl) {
		imageUrl = imageUrl.substring(0, imageUrl.lastIndexOf('.'));
		int resID = context.getResources().getIdentifier(imageUrl, "drawable",  context.getPackageName());
		imageView.setImageResource(resID);
	}

}
