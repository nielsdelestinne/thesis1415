package niels.master.thesis.prototypev2.activities.func1;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.R.anim;
import niels.master.thesis.prototypev2.R.id;
import niels.master.thesis.prototypev2.R.layout;
import niels.master.thesis.prototypev2.R.menu;
import niels.master.thesis.prototypev2.R.string;
import niels.master.thesis.prototypev2.entities.FactBlock;
import niels.master.thesis.prototypev2.entities.ImageBlock;
import niels.master.thesis.prototypev2.entities.MapBlock;
import niels.master.thesis.prototypev2.entities.QuizBlock;
import niels.master.thesis.prototypev2.fragments.FactBlockFragment;
import niels.master.thesis.prototypev2.fragments.ImageBlockFragment;
import niels.master.thesis.prototypev2.fragments.MapBlockFragment;
import niels.master.thesis.prototypev2.fragments.QuizBlockFragment;
import niels.master.thesis.prototypev2.functionality.ActivityExtender;
import niels.master.thesis.prototypev2.functionality.SynchronizationHandler;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class InformationBlockActivity extends ActivityExtender {

	
	public static Context CONTEXT;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_information_block);

		/**
		 * -------------------
		 * MAIN CODE EXECUTION
		 * -------------------
		 */
		CONTEXT = this;
		fullScreenMode();
		
		/**
		 * -------------------
		 * -------------------
		 */
		
//		View decorView = getWindow().getDecorView();
//		// Hide both the navigation bar and the status bar.
//		// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
//		// a general rule, you should design your app to hide the status bar whenever you
//		// hide the navigation bar.
//		int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//		              | View.SYSTEM_UI_FLAG_FULLSCREEN;
//		decorView.setSystemUiVisibility(uiOptions);

	}
	
	@Override
	protected void onStart() {
		super.onStart();
		// RECIEVAL OF INTENT
		Intent intent = getIntent();
		String[] messageReceived = intent.getStringArrayExtra(SynchronizationHandler.PASSED_MESSAGE);
		//int id = Integer.parseInt(messageReceived[0]);
		String type = messageReceived[1];
		String hasNextUpButton = messageReceived[2];
		String hasPreviousButton = messageReceived[3];
		// LOAD CORRECT FRAGMENT
		try {
			Fragment fragment = selectFragment(type);
			fragment.setArguments(intent.getExtras());
			getFragmentManager().beginTransaction()
            .add(R.id.fragment_container, fragment).commit();
		} catch (Exception e) {
			Log.e(null, e.getMessage());
		}

		// NEXT - PREVIOUS FUNCTIONALITY
		managePreviousNextControls(hasNextUpButton, hasPreviousButton);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.title_bar_homeicon_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.home:
	            launchHome();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void launchHome() {
		showDialog();
	}
	
	/**
	 * Method that shows the dialog
	 */
	private void showDialog() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(InformationBlockActivity.CONTEXT);
		
		// set title
		alertDialogBuilder.setTitle(R.string.title_home);

		// set dialog message
		alertDialogBuilder
			.setMessage(R.string.message_home)
			.setCancelable(false)
			.setPositiveButton(R.string.btn_home,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					SynchronizationHandler sh = SynchronizationHandler.getInstance(0);
					sh.reset();
					Intent intent = new Intent(CONTEXT, HomeScreenActivity.class);
					intent.putExtra(HomeScreenActivity.MODE_MESSAGE, true);
					CONTEXT.startActivity(intent);
					overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);	
				}
			})
			.setNegativeButton(R.string.btn_cancel,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
	
	
	/**
	 * Creates and returns a subclass object of Fragment
	 * based on the specified Type "type".
	 * @param type
	 * @return Newly created Fragment object
	 * @throws Exception
	 */
	private Fragment selectFragment(String type) throws Exception {
		if(type.equals(FactBlock.TABLE_NAME))
			return new FactBlockFragment();
		if(type.equals(ImageBlock.TABLE_NAME))
			return new ImageBlockFragment();
		if(type.equals(MapBlock.TABLE_NAME))
			return new MapBlockFragment();
		if(type.equals(QuizBlock.TABLE_NAME))
			return new QuizBlockFragment();
		throw new Exception("InformationBlockActivity - selectFragment - forgot to add newly added fragment");
	}
	
	public void previous(View view) {
		SynchronizationHandler sync = SynchronizationHandler.getInstance(0);
		Intent intent = sync.launchPreviousInformationBlock(false);
		if(intent != null) {
			startActivity(intent);
			overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);		
		}
	}
	
	public void next(View view) {
		SynchronizationHandler sync = SynchronizationHandler.getInstance(0);
		Intent intent = sync.launchNextInformationBlock();
		if(intent != null) {
			startActivity(intent);
			overridePendingTransition(R.anim.default_activity_animation_in2, R.anim.default_activity_animation_out2);			
		}
	}
	
	public void finish(View view) {
		Intent intent = new Intent(CONTEXT, HomeScreenActivity.class);
		intent.putExtra(HomeScreenActivity.MODE_MESSAGE, true);
		CONTEXT.startActivity(intent);
		overridePendingTransition(R.anim.default_activity_animation_in2, R.anim.default_activity_animation_out2);	
	}
	
	/**
	 * Method for managing the visibility and text to show for the
	 * previous and next button
	 * @param hasNextUpButton
	 * @param hasPreviousButton
	 */
	private void managePreviousNextControls(String hasNextUpButton, String hasPreviousButton) {
		if(hasNextUpButton.equals("1")) {
			Button btnNext = (Button) findViewById(R.id.btnNext);
			btnNext.setText(R.string.btn_nextup);
		}
		else if(hasNextUpButton.equals("-1")) {
			// HIDE THE NEXT BUTTON
			Button btnNext = (Button) findViewById(R.id.btnNext);
			btnNext.setVisibility(View.INVISIBLE);
			// SHOW THE VISIBLE BUTTON
			Button btnFinish = (Button) findViewById(R.id.btnFinish);
			btnFinish.setVisibility(View.VISIBLE);
			
		}
		if(hasPreviousButton.equals("-1")) {
			Button btnPrev = (Button) findViewById(R.id.btnPrevious);
			btnPrev.setVisibility(View.INVISIBLE);
		}
	}
	
}
