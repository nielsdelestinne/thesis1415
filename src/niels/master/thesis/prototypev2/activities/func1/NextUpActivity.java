package niels.master.thesis.prototypev2.activities.func1;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.R.anim;
import niels.master.thesis.prototypev2.R.id;
import niels.master.thesis.prototypev2.R.layout;
import niels.master.thesis.prototypev2.R.menu;
import niels.master.thesis.prototypev2.R.string;
import niels.master.thesis.prototypev2.functionality.ActivityExtender;
import niels.master.thesis.prototypev2.functionality.SynchronizationHandler;
import niels.master.thesis.prototypev2.util.SystemUiHider;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.widget.FrameLayout.LayoutParams;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NextUpActivity extends ActivityExtender {

	public static Context CONTEXT;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_next_up);
		
		/**
		 * -------------------
		 * MAIN CODE EXECUTION
		 * -------------------
		 */
		CONTEXT = this;
		fullScreenMode();
		Intent intent = getIntent();
		String[] messageReceived = intent.getStringArrayExtra(SynchronizationHandler.PASSED_MESSAGE);
		String title = messageReceived[0];
		int startTime = Integer.parseInt(messageReceived[1]);
		int currentTime = Integer.parseInt(messageReceived[2]); 
		
		TextView titleNextUp = (TextView) findViewById(R.id.titleNextUpSubject);
		titleNextUp.setText(title);
		TextView txtCounterNextUp = (TextView) findViewById(R.id.counterNextUp);
		int remainingTime = startTime - currentTime;
		txtCounterNextUp.setText("In " + remainingTime);
		nextUpCounter(txtCounterNextUp, remainingTime);
		
		String hasPreviousButton = messageReceived[3];
		if(hasPreviousButton.equals("-1")) {
			Button btnPrev = (Button) findViewById(R.id.btnPrevious);
			btnPrev.setVisibility(View.INVISIBLE);
		}
		
//		View decorView = getWindow().getDecorView();
//		// Hide both the navigation bar and the status bar.
//		// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
//		// a general rule, you should design your app to hide the status bar whenever you
//		// hide the navigation bar.
//		int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
//		decorView.setSystemUiVisibility(uiOptions);
		
		/**
		 * -------------------
		 * -------------------
		 */
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.title_bar_homeicon_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.home:
	            launchHome();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void launchHome() {
		showDialog();
	}
	
	/**
	 * Method that shows the dialog
	 */
	private void showDialog() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NextUpActivity.CONTEXT);
		
		// set title
		alertDialogBuilder.setTitle(R.string.title_home);

		// set dialog message
		alertDialogBuilder
			.setMessage(R.string.message_home)
			.setCancelable(false)
			.setPositiveButton(R.string.btn_home,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					SynchronizationHandler sh = SynchronizationHandler.getInstance(0);
					sh.reset();
					Intent intent = new Intent(CONTEXT, HomeScreenActivity.class);
					intent.putExtra(HomeScreenActivity.MODE_MESSAGE, true);
					CONTEXT.startActivity(intent);
					overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);	
				}
			})
			.setNegativeButton(R.string.btn_cancel,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
	
	/**
	 * Method that creates the counter and that
	 * counts down until the remainigTime == 0
	 * @param txtCounterNextUp
	 * @param remainingTime
	 */
	private void nextUpCounter(final TextView txtCounterNextUp, int remainingTime) {
		new CountDownTimer((remainingTime + 1) * 1000, SynchronizationHandler.INTERVAL) {

		     public void onTick(long millisUntilFinished) {
		    	 txtCounterNextUp.setText("seconds remaining: " + millisUntilFinished / 1000);
		     }

		     public void onFinish() {
		         // nothing
		     }
		  }.start();
	}
	
	public void previous(View view) {
		SynchronizationHandler sync = SynchronizationHandler.getInstance(0);
		Intent intent = sync.launchPreviousInformationBlock(true);
		if(intent != null) {
			startActivity(intent);
			overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);			
		}
	}
}
