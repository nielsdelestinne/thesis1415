package niels.master.thesis.prototypev2.activities.func1;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.R.anim;
import niels.master.thesis.prototypev2.R.id;
import niels.master.thesis.prototypev2.R.layout;
import niels.master.thesis.prototypev2.R.menu;
import niels.master.thesis.prototypev2.functionality.ActivityExtender;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class HelpScreenActivity extends ActivityExtender {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help_screen);
		fullScreenMode();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.title_bar_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.home:
	            finish();
	            overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);	
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
