package niels.master.thesis.prototypev2.activities.func1;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.R.anim;
import niels.master.thesis.prototypev2.R.id;
import niels.master.thesis.prototypev2.R.layout;
import niels.master.thesis.prototypev2.R.menu;
import niels.master.thesis.prototypev2.entities.FactBlock;
import niels.master.thesis.prototypev2.entities.ImageBlock;
import niels.master.thesis.prototypev2.entities.MapBlock;
import niels.master.thesis.prototypev2.entities.QuizBlock;
import niels.master.thesis.prototypev2.fragments.FactBlockFragment;
import niels.master.thesis.prototypev2.fragments.ImageBlockFragment;
import niels.master.thesis.prototypev2.fragments.MapBlockFragment;
import niels.master.thesis.prototypev2.fragments.QuizBlockFragment;
import niels.master.thesis.prototypev2.functionality.ActivityExtender;
import niels.master.thesis.prototypev2.functionality.SynchronizationHandler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class SimpleInformationBlockActivity extends ActivityExtender {

	public static final String FRAGMENT_CONTENT_MESSAGE = SynchronizationHandler.PASSED_MESSAGE;
	public static Context CONTEXT;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_simple_information_block);

		/**
		 * -------------------
		 * MAIN CODE EXECUTION
		 * -------------------
		 */
		CONTEXT = this;
		fullScreenMode();
		
		// RECIEVAL OF INTENT
		Intent intent = getIntent();
		String[] messageReceived = intent.getStringArrayExtra(FRAGMENT_CONTENT_MESSAGE);
		//int id = Integer.parseInt(messageReceived[0]);
		String type = messageReceived[1];
		// LOAD CORRECT FRAGMENT
		try {
			Fragment fragment = selectFragment(type);
			fragment.setArguments(intent.getExtras());
			getFragmentManager().beginTransaction()
            .add(R.id.fragment_container, fragment).commit();
		} catch (Exception e) {
			Log.e(null, e.getMessage());
		}
		
		/**
		 * -------------------
		 * -------------------
		 */

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.title_bar_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.home:
	            finish();
	            overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);	
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	
	
	/**
	 * Creates and returns a subclass object of Fragment
	 * based on the specified Type "type".
	 * @param type
	 * @return Newly created Fragment object
	 * @throws Exception
	 */
	private Fragment selectFragment(String type) throws Exception {
		if(type.equals(FactBlock.TABLE_NAME))
			return new FactBlockFragment();
		if(type.equals(ImageBlock.TABLE_NAME))
			return new ImageBlockFragment();
		if(type.equals(MapBlock.TABLE_NAME))
			return new MapBlockFragment();
		if(type.equals(QuizBlock.TABLE_NAME))
			return new QuizBlockFragment();
		throw new Exception("InformationBlockActivity - selectFragment - forgot to add newly added fragment");
	}

}
