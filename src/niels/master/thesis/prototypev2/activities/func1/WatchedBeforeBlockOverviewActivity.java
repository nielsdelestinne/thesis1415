package niels.master.thesis.prototypev2.activities.func1;

import java.util.ArrayList;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.R.anim;
import niels.master.thesis.prototypev2.R.id;
import niels.master.thesis.prototypev2.R.layout;
import niels.master.thesis.prototypev2.R.menu;
import niels.master.thesis.prototypev2.R.string;
import niels.master.thesis.prototypev2.database.DatabaseAdapter;
import niels.master.thesis.prototypev2.entities.InformationBlock;
import niels.master.thesis.prototypev2.functionality.ActivityExtender;
import niels.master.thesis.prototypev2.functionality.WatchedBeforeBlockAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class WatchedBeforeBlockOverviewActivity extends ActivityExtender {

	public static final String DOC_MESSAGE = "DOC_MESSAGE";
	private Context CONTEXT;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_watched_before_block_overview);
		
		/**
		 * -------------------
		 * MAIN CODE EXECUTION
		 * -------------------
		 */
		CONTEXT = this;
		fullScreenMode();
		
		// INTENT
		Intent intent = getIntent();
		String[] extra = intent.getStringArrayExtra(DOC_MESSAGE);
		int docId = Integer.parseInt(extra[0]);
		String docName = extra[1];
		
		// DATA
		ArrayList<InformationBlock> informationBlocks = loadInformationBlocks(docId);
		
		// SET TEXT
		TextView watchedBefore = (TextView) findViewById(R.id.titleWatchedBefore);
		watchedBefore.setText(R.string.title_watchedbeforeblock);
		TextView watchedBeforeDoc = (TextView) findViewById(R.id.watchedBeforeDocName);
		watchedBeforeDoc.setText("From documentary: " + docName);
		// GRIDVIEW
		GridView gridView = (GridView) findViewById(R.id.gridViewWatchedBefore);
		final InformationBlock[] informationBlockArray = informationBlocks.toArray(new InformationBlock[informationBlocks.size()]);
		gridView.setAdapter(new WatchedBeforeBlockAdapter(this, informationBlockArray));
		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				Toast.makeText(
				   getApplicationContext(),
				   ((TextView) v.findViewById(R.id.grid_item_label))
				   .getText(), Toast.LENGTH_SHORT).show();
				
				// START THE WATCHERBEFOREBLOCKOVERVIEW ACTIVITY
				Intent intent = new Intent(CONTEXT, SimpleInformationBlockActivity.class);
				String[] extra = {informationBlockArray[position].getId()+"", informationBlockArray[position].getType()};
				intent.putExtra(SimpleInformationBlockActivity.FRAGMENT_CONTENT_MESSAGE, extra);
				CONTEXT.startActivity(intent);
				overridePendingTransition(R.anim.default_activity_animation_in2, R.anim.default_activity_animation_out2);
 
			}
		});
	}
	
	/**
	 * Loads the information blocks from the database and places it in the memory.
	 * InformationBlock objects are created
	 */
	private ArrayList<InformationBlock> loadInformationBlocks(int docId) {
		DatabaseAdapter dba = new DatabaseAdapter(HomeScreenActivity.CONTEXT);
		dba.open();
		ArrayList<InformationBlock> informationBlocks = null;
		try {
			informationBlocks = dba.getInformationBlocksForSpecifiedDocumentary(docId);
		} catch (Exception e) {
			Log.e(null, e.getMessage());
			e.printStackTrace();
		}
		dba.close();
		return informationBlocks;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.title_bar_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.home:
	            finish();
	            overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);	
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
