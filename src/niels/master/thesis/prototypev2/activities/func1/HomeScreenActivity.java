package niels.master.thesis.prototypev2.activities.func1;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.R.anim;
import niels.master.thesis.prototypev2.R.id;
import niels.master.thesis.prototypev2.R.layout;
import niels.master.thesis.prototypev2.R.menu;
import niels.master.thesis.prototypev2.R.string;
import niels.master.thesis.prototypev2.activities.func2.StoryboardActivity;
import niels.master.thesis.prototypev2.activities.func2.StoryboardPlayingFragmentsActivity;
import niels.master.thesis.prototypev2.database.DatabaseAdapter;
import niels.master.thesis.prototypev2.database.DatabaseBootstrapper;
import niels.master.thesis.prototypev2.entities.Documentary;
import niels.master.thesis.prototypev2.functionality.ActivityExtender;
import niels.master.thesis.prototypev2.functionality.SynchronizationHandler;
import niels.master.thesis.prototypev2.functionality.WatchedBeforeDocumentaryAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

public class HomeScreenActivity extends ActivityExtender {
	
	public static Context CONTEXT;
	public static final String MODE_MESSAGE = "MODE_MESSAGE";
	
	GridView gridView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homescreen);
		
		Intent intent = getIntent();
		boolean isAfterMode = intent.getBooleanExtra(MODE_MESSAGE, false);
		
		// TABS
		TabHost myTabHost =(TabHost) findViewById(android.R.id.tabhost);
		myTabHost.setup();
		myTabHost.addTab(myTabHost.newTabSpec("tab_inser").setIndicator("PLAYING NOW",
				getResources().getDrawable(android.R.drawable.ic_menu_edit)).setContent(R.id.tab1));
		myTabHost.addTab(myTabHost.newTabSpec("tab_inser").setIndicator("WATCHED BEFORE",
				getResources().getDrawable(android.R.drawable.ic_dialog_info)).setContent(R.id.tab2));
		
		/**
		 * -------------------
		 * MAIN CODE EXECUTION
		 * -------------------
		 */
		CONTEXT = this;
		fullScreenMode();
		bootstrapDatabase();
		
		// PLAYING NOW VS. WATCHED BEFORE CONTENT
		// the homescreen is different when we start
		// the application compared to when the synchronization is complete
		if(isAfterMode)
			afterMode();
		else beforeMode();
		
		/**
		 * -------------------
		 * -------------------
		 */

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.home_bar_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.information:
	        	// START THE HELPSCREEN ACTIVITY
				Intent intent = new Intent(CONTEXT, HelpScreenActivity.class);
				CONTEXT.startActivity(intent);
				overridePendingTransition(R.anim.default_activity_animation_in2, R.anim.default_activity_animation_out2);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void beforeMode() {

		Documentary documentary = loadActiveDocumentary();
		imageTest(true);
		
		/**
		 * PLAYING NOW TAB
		 */
		TextView titleDocNextUp = (TextView) findViewById(R.id.titleDocumentaryNextUp);
		TextView docDescription = (TextView) findViewById(R.id.docDescription);
		titleDocNextUp.setText(documentary.getName());
		docDescription.setText(documentary.getDescription());
		
		/**
		 * WATCHED BEFORE TAB
		 */
		ArrayList<Documentary> documentaries = loadPreviouslyWatchedDocumentaries();
		
		// IF THERE ARE PREVIOUSLY WATCHED DOCUMENTARIES
		if(!documentaries.isEmpty()) {
			
			// SET THE CORRECT TITLE
			TextView watchedBefore = (TextView) findViewById(R.id.titleWatchedBefore);
			watchedBefore.setText(R.string.title_watchedbefore);
			
			// HIDE THE "please watch anything" TEXTVIEW
			TextView watchedBeforeNone = (TextView) findViewById(R.id.watchedBeforeNone);
			watchedBeforeNone.setVisibility(View.INVISIBLE);
			
			// LOAD AND FILL THE GRIDVIEW
			loadGridView(documentaries);
		}
		else {
			
			// SET THE CORRECT TITLE
			TextView watchedBefore = (TextView) findViewById(R.id.titleWatchedBefore);
			watchedBefore.setText(R.string.title_watchedbefore_none);
			
			// MAKE SURE THE "please watch anything" TEXTVIEW IS VISIBLE
			TextView watchedBeforeNone = (TextView) findViewById(R.id.watchedBeforeNone);
			watchedBeforeNone.setVisibility(View.VISIBLE);
		}
		
		
	}
	
	private void afterMode() {
		
		imageTest(false);
		
		/**
		 * PLAYING NOW TAB
		 */
		TextView titleDocNextUp = (TextView) findViewById(R.id.titleDocumentaryNextUp);
		titleDocNextUp.setText(R.string.title_noActiveDoc);
		TextView docDescription = (TextView) findViewById(R.id.docDescription);
		docDescription.setText("Open ocean, a vast biotope covering two thirds "
				+ "of the planet, some shallow, some as deep as the mountain "
				+ "ranges are high. The ocean has an immense, precariously complex "
				+ "food chain, varying from microscopic animals, like krill, to whales, "
				+ "which ironically feed mainly on the former. Most species swim or float "
				+ "in it, many coming up for air, while other dive in from land or air, "
				+ "often to feed, but also to procreate on the coast, where some species "
				+ "come to lay their eggs. Even the shore is covered with life, largely "
				+ "based on organic matter, such as corpses.");
		// HIDE THE START BUTTON
		//Button btn = (Button) findViewById(R.id.btnStartSync);
		//btn.setVisibility(View.INVISIBLE);
		
		/**
		 * WATCHED BEFORE TAB
		 */
		TextView watchedBefore = (TextView) findViewById(R.id.titleWatchedBefore);
		watchedBefore.setText(R.string.title_watchedbefore);
		ArrayList<Documentary> documentaries = loadPreviouslyWatchedDocumentaries();
		
		// HIDE THE "please watch anything" TEXTVIEW 
		TextView watchedBeforeNone = (TextView) findViewById(R.id.watchedBeforeNone);
		watchedBeforeNone.setVisibility(View.INVISIBLE);
		
		// LOAD THE ACTIVE DOCUMENTARY AND ADD IT TO THE LIST OF WATCHED DOCUMENTARIES
		Documentary documentary = loadActiveDocumentary();
		documentaries.add(documentary);
		
		// LOAD AND FILL THE GRIDVIEW
		loadGridView(documentaries);
		
		
	}
	
	/**
	 * Load and fill the gridview
	 * @param documentaries
	 */
	private void loadGridView(ArrayList<Documentary> documentaries) {
		gridView = (GridView) findViewById(R.id.gridViewWatchedBefore);
		final Documentary[] documentariesArray = documentaries.toArray(new Documentary[documentaries.size()]);
		gridView.setAdapter(new WatchedBeforeDocumentaryAdapter(this, documentariesArray));
		gridView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
//				Toast.makeText(
//				   getApplicationContext(),
//				   ((TextView) v.findViewById(R.id.grid_item_label))
//				   .getText(), Toast.LENGTH_SHORT).show();
				
				// START THE WATCHERBEFOREBLOCKOVERVIEW ACTIVITY
				Intent intent = new Intent(CONTEXT, WatchedBeforeBlockOverviewActivity.class);
				String[] extra = {documentariesArray[position].getId()+"", documentariesArray[position].getName()};
				intent.putExtra(WatchedBeforeBlockOverviewActivity.DOC_MESSAGE, extra);
				CONTEXT.startActivity(intent);
				overridePendingTransition(R.anim.default_activity_animation_in2, R.anim.default_activity_animation_out2);
 
			}
		});
	}
	
	/**
	 * Copies the sqlite database to the storage (SD) of the connected device
	 * if it is not already present
	 */
	private void bootstrapDatabase() {
		DatabaseBootstrapper databaseBootstrapper = 
				new DatabaseBootstrapper(getBaseContext(), getPackageName());
		databaseBootstrapper.storeDatabaseOnDevice();
	}
	
	/**
	 * Loads the active documentary from the database and places it in the memory.
	 */
	private Documentary loadActiveDocumentary() {
		DatabaseAdapter dba = new DatabaseAdapter(getBaseContext());
		dba.open();
		Documentary documentary = dba.getSingleDocumentaryById(
				Documentary.CURRENT_DOC_ID, Documentary.TABLE_NAME, Documentary.COLUMNS);
		dba.close();
		return documentary;
	}
	
	/**
	 * Loads the previously watched documentaries and places it in the memory.
	 */
	private ArrayList<Documentary> loadPreviouslyWatchedDocumentaries() {
		ArrayList<Documentary> documentaries = new ArrayList<Documentary>();
		DatabaseAdapter dba = new DatabaseAdapter(getBaseContext());
		dba.open();
		documentaries = dba.getWatchedBeforeDocumentaries(Documentary.TABLE_NAME, Documentary.COLUMNS);
		dba.close();
		return documentaries;
	}
	
	/**
	 * TODO: REWRITE THIS IMAGE STUFF
	 */
	private void imageTest(boolean isBefore) {
		Resources res = getResources();
		AssetManager am = res.getAssets();
		InputStream iS;
		try {
			if(isBefore) iS = am.open("images/doc_1.png");
			else iS = am.open("images/doc_x.png");
			ImageView imgView = (ImageView) findViewById(R.id.docImageView);
			imgView.setImageBitmap(BitmapFactory.decodeStream(iS));   
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void startSynchronization(View view) {
		// SETS THE STATIC CONTEXT VAR
		new HomeScreenActivity();
		new NextUpActivity();
		// STARTS THE SYNCHRONIZATION
		SynchronizationHandler.getInstance(1000);
	}
	
	public void startStoryboard(View view) {
		Intent intent = new Intent(this, StoryboardActivity.class);
		intent.putExtra(WatchedBeforeBlockOverviewActivity.DOC_MESSAGE, Documentary.CURRENT_DOC_ID);
		startActivity(intent);
	}
	
}
