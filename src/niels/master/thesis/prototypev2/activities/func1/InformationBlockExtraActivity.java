package niels.master.thesis.prototypev2.activities.func1;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.R.anim;
import niels.master.thesis.prototypev2.R.id;
import niels.master.thesis.prototypev2.R.layout;
import niels.master.thesis.prototypev2.R.menu;
import niels.master.thesis.prototypev2.R.string;
import niels.master.thesis.prototypev2.activities.func2.StoryboardPlayingFragmentsActivity;
import niels.master.thesis.prototypev2.aws.sqs.MySQS2;
import niels.master.thesis.prototypev2.entities.FactBlock;
import niels.master.thesis.prototypev2.entities.ImageBlock;
import niels.master.thesis.prototypev2.entities.MapBlock;
import niels.master.thesis.prototypev2.entities.QuizBlock;
import niels.master.thesis.prototypev2.fragments.FactBlockFragment;
import niels.master.thesis.prototypev2.fragments.ImageBlockFragment;
import niels.master.thesis.prototypev2.fragments.MapBlockFragment;
import niels.master.thesis.prototypev2.fragments.QuizBlockFragment;
import niels.master.thesis.prototypev2.functionality.ActivityExtender;
import niels.master.thesis.prototypev2.functionality.SynchronizationHandler;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class InformationBlockExtraActivity extends ActivityExtender {

	
	public static Context CONTEXT;
	private MySQS2 mySQSSending = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_information_block_extra);

		/**
		 * -------------------
		 * MAIN CODE EXECUTION
		 * -------------------
		 */
		CONTEXT = this;
		fullScreenMode();
		
		// SQS sending init
		mySQSSending = new MySQS2();
		
		redrawElements();
		
		
		// Volume Seekbar
		SeekBar seekBar = (SeekBar) findViewById(R.id.sliderVolume);
		TextView textView = (TextView) findViewById(R.id.txtVolume);
		textView.setText("Volume: " + seekBar.getProgress());
	  
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			  TextView textView = (TextView) findViewById(R.id.txtVolume);
			  
			  @Override
			  public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
				  StoryboardPlayingFragmentsActivity.progress = progresValue;
				  textView.setText("Volume: " + StoryboardPlayingFragmentsActivity.progress);
			  }
			
			  @Override
			  public void onStartTrackingTouch(SeekBar seekBar) {
				  // do nothing special
			  }
			
			  @Override
			  public void onStopTrackingTouch(SeekBar seekBar) {
				  textView.setText("Volume: " + StoryboardPlayingFragmentsActivity.progress);
				  mySQSSending.sendMessageToQueue(StoryboardPlayingFragmentsActivity.CMD_MESSAGE_VOLUME + StoryboardPlayingFragmentsActivity.progress);
			  }
	    });
		
		/**
		 * -------------------
		 * -------------------
		 */
		
//		View decorView = getWindow().getDecorView();
//		// Hide both the navigation bar and the status bar.
//		// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
//		// a general rule, you should design your app to hide the status bar whenever you
//		// hide the navigation bar.
//		int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//		              | View.SYSTEM_UI_FLAG_FULLSCREEN;
//		decorView.setSystemUiVisibility(uiOptions);

	}
	
	/**
	 * Redraw the screen elements
	 */
	private void redrawElements() {
		TextView txtStatus = (TextView) findViewById(R.id.txtStatus);
		txtStatus.setText("Showing information...");
		SeekBar seekBar = (SeekBar) findViewById(R.id.sliderVolume);
		TextView textView = (TextView) findViewById(R.id.txtVolume);
		textView.setText("Volume: " + StoryboardPlayingFragmentsActivity.progress);
		seekBar.setProgress(StoryboardPlayingFragmentsActivity.progress);
		ImageButton btnMute = (ImageButton) findViewById(R.id.btnMute);
		if(StoryboardPlayingFragmentsActivity.isMuted)
			btnMute.setImageResource(R.drawable.ic_ctrl_sound_mute);
		else btnMute.setImageResource(R.drawable.ic_ctrl_sound_active);
		ImageButton btn = (ImageButton) findViewById(R.id.btnPlay);
		if(!StoryboardPlayingFragmentsActivity.isPlaying)
			btn.setImageResource(R.drawable.ic_ctrl_play);
		else btn.setImageResource(R.drawable.ic_ctrl_pause);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		// RECIEVAL OF INTENT
		Intent intent = getIntent();
		String[] messageReceived = intent.getStringArrayExtra(SynchronizationHandler.PASSED_MESSAGE);
		//int id = Integer.parseInt(messageReceived[0]);
		String type = messageReceived[1];
		// LOAD CORRECT FRAGMENT
		try {
			Fragment fragment = selectFragment(type);
			fragment.setArguments(intent.getExtras());
			getFragmentManager().beginTransaction()
            .add(R.id.fragment_container, fragment).commit();
		} catch (Exception e) {
			Log.e(null, e.getMessage());
		}

		// NEXT - PREVIOUS FUNCTIONALITY
		//managePreviousNextControls(hasNextUpButton, hasPreviousButton);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.title_bar_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.home:
	            launchHome();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private void launchHome() {
		finish();
		overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);	
		//showDialog();
	}
	
	public void ctrlPrevious(View view) {
		if(StoryboardPlayingFragmentsActivity.active_index > 0) {
			StoryboardPlayingFragmentsActivity.active_index--;
			mySQSSending.sendMessageToQueue(StoryboardPlayingFragmentsActivity.CMD_ITEM_PLAY + StoryboardPlayingFragmentsActivity.active_index);
			// Automatically starts playing, so update play button
			if(!StoryboardPlayingFragmentsActivity.isPlaying) {
				ImageButton btn = (ImageButton) findViewById(R.id.btnPlay);
				btn.setImageResource(R.drawable.ic_ctrl_pause);
				StoryboardPlayingFragmentsActivity.isPlaying = !StoryboardPlayingFragmentsActivity.isPlaying;
			}
		}
		else {
			System.out.println("No more fragments [previous]");
		}
	}
	
	public void ctrlNext(View view) {
		if(StoryboardPlayingFragmentsActivity.active_index < StoryboardPlayingFragmentsActivity.fragments.size()) {
			StoryboardPlayingFragmentsActivity.active_index++;
			playFragmentWithActiveIndex();
		}
		else {
			System.out.println("No more fragments [next]");
		}
	}
	
	/**
	 * Send the message to play the fragment which index equals the active_index
	 */
	private void playFragmentWithActiveIndex() {
		mySQSSending.sendMessageToQueue(StoryboardPlayingFragmentsActivity.CMD_ITEM_PLAY + StoryboardPlayingFragmentsActivity.active_index);
		// Automatically starts playing, so update play button
		if(!StoryboardPlayingFragmentsActivity.isPlaying) {
			ImageButton btn = (ImageButton) findViewById(R.id.btnPlay);
			btn.setImageResource(R.drawable.ic_ctrl_pause);
			StoryboardPlayingFragmentsActivity.isPlaying = !StoryboardPlayingFragmentsActivity.isPlaying;
		}
	}
	
	public void ctrlPlay(View view) {
		ImageButton btn = (ImageButton) findViewById(R.id.btnPlay);
		// If pause button was showed before clicking
		if(StoryboardPlayingFragmentsActivity.isPlaying) {
			btn.setImageResource(R.drawable.ic_ctrl_play);
			mySQSSending.sendMessageToQueue(StoryboardPlayingFragmentsActivity.CMD_MESSAGE_PAUSE);
		}
		// Else play button was showed
		else {
			btn.setImageResource(R.drawable.ic_ctrl_pause);
			mySQSSending.sendMessageToQueue(StoryboardPlayingFragmentsActivity.CMD_MESSAGE_PLAY);
			// send sqs message
		}
		StoryboardPlayingFragmentsActivity.isPlaying = !StoryboardPlayingFragmentsActivity.isPlaying;
	}
	
	public void ctrlMute(View view) {
		ImageButton btn = (ImageButton) findViewById(R.id.btnMute);
		// If audio button was showed before clicking
		//  we are going to mute the audio
		if(!StoryboardPlayingFragmentsActivity.isMuted) {
			btn.setImageResource(R.drawable.ic_ctrl_sound_mute);
			// Volume Seekbar
			SeekBar seekBar = (SeekBar) findViewById(R.id.sliderVolume);
			TextView textView = (TextView) findViewById(R.id.txtVolume);
			seekBar.setProgress(0);
			textView.setText("Volume: 0");
			// send sqs message
			mySQSSending.sendMessageToQueue(StoryboardPlayingFragmentsActivity.CMD_MESSAGE_MUTE_ON);
		}
		// Else unmute button was showed
		else {
			btn.setImageResource(R.drawable.ic_ctrl_sound_active);
			// Volume Seekbar
			SeekBar seekBar = (SeekBar) findViewById(R.id.sliderVolume);
			TextView textView = (TextView) findViewById(R.id.txtVolume);
			seekBar.setProgress(100);
			textView.setText("Volume: 100");
			// send sqs message
			mySQSSending.sendMessageToQueue(StoryboardPlayingFragmentsActivity.CMD_MESSAGE_MUTE_OFF);
		}
		StoryboardPlayingFragmentsActivity.isMuted = !StoryboardPlayingFragmentsActivity.isMuted;
	}
	
	public void ctrlEdit(View view) {
		//Intent intent = new Intent(this, StoryboardActivity.class);
		//intent.putExtra(WatchedBeforeBlockOverviewActivity.DOC_MESSAGE, Documentary.CURRENT_DOC_ID);
		//startActivity(intent);
		this.finish();
		overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);
	}
	
	/**
	 * Method that shows the dialog
	 */
	private void showDialog() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(InformationBlockExtraActivity.CONTEXT);
		
		// set title
		alertDialogBuilder.setTitle(R.string.title_home);

		// set dialog message
		alertDialogBuilder
			.setMessage(R.string.message_home)
			.setCancelable(false)
			.setPositiveButton(R.string.btn_home,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int id) {
					SynchronizationHandler sh = SynchronizationHandler.getInstance(0);
					sh.reset();
					Intent intent = new Intent(CONTEXT, HomeScreenActivity.class);
					intent.putExtra(HomeScreenActivity.MODE_MESSAGE, true);
					CONTEXT.startActivity(intent);
					overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);	
				}
			})
			.setNegativeButton(R.string.btn_cancel,new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
	
	
	/**
	 * Creates and returns a subclass object of Fragment
	 * based on the specified Type "type".
	 * @param type
	 * @return Newly created Fragment object
	 * @throws Exception
	 */
	private Fragment selectFragment(String type) throws Exception {
		if(type.equals(FactBlock.TABLE_NAME))
			return new FactBlockFragment();
		if(type.equals(ImageBlock.TABLE_NAME))
			return new ImageBlockFragment();
		if(type.equals(MapBlock.TABLE_NAME))
			return new MapBlockFragment();
		if(type.equals(QuizBlock.TABLE_NAME))
			return new QuizBlockFragment();
		throw new Exception("InformationBlockActivity - selectFragment - forgot to add newly added fragment");
	}
	
}
