package niels.master.thesis.prototypev2.activities.func2;

import niels.master.thesis.prototypev2.entities.StoryboardFragment;
import android.view.View;

public interface BtnClickListener {
    public abstract void onBtnClick(StoryboardFragment storyboardFragment, View view);
}