package niels.master.thesis.prototypev2.activities.func2;

import java.util.ArrayList;
import java.util.Collections;

import com.jess.ui.TwoWayAdapterView;
import com.jess.ui.TwoWayGridView;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.activities.func1.HomeScreenActivity;
import niels.master.thesis.prototypev2.activities.func1.SimpleInformationBlockActivity;
import niels.master.thesis.prototypev2.activities.func1.WatchedBeforeBlockOverviewActivity;
import niels.master.thesis.prototypev2.database.DatabaseAdapter;
import niels.master.thesis.prototypev2.entities.Documentary;
import niels.master.thesis.prototypev2.entities.InformationBlock;
import niels.master.thesis.prototypev2.entities.StoryboardFragment;
import niels.master.thesis.prototypev2.functionality.ActivityExtender;
import niels.master.thesis.prototypev2.functionality.StoryboardFragmentAdapter;
import niels.master.thesis.prototypev2.functionality.WatchedBeforeBlockAdapter;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class StoryboardActivity extends ActivityExtender {

	public static final String DOC_MESSAGE = "DOC_MESSAGE";
	private Context CONTEXT;
	private StoryboardFragmentAdapter storyboardFragmentAdapter;
	private ArrayList<StoryboardFragment> storyboardFragments;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_storyboard);
		
		/**
		 * -------------------
		 * MAIN CODE EXECUTION
		 * -------------------
		 */
		CONTEXT = this;
		fullScreenMode();
		
		// INTENT
		Intent intent = getIntent();
		int docId = intent.getIntExtra(DOC_MESSAGE, 1);
		
		// DATA
		DatabaseAdapter dba = new DatabaseAdapter(this);
		dba.open();
		storyboardFragments = dba.getStoryboardFragmentsForSpecifiedDocumentary(docId);
		dba.close();
		
		// GRIDVIEW
		TwoWayGridView gridView = (TwoWayGridView) findViewById(R.id.gridViewWatchedBefore);
		Button btnStart = (Button) findViewById(R.id.btnStartStoryboard);
		final StoryboardFragment[] storyboardFragmentsArray = storyboardFragments.toArray(new StoryboardFragment[storyboardFragments.size()]);
		storyboardFragmentAdapter = new StoryboardFragmentAdapter(this, btnStart, storyboardFragmentsArray, showInformationListener(), gridView);
		gridView.setAdapter(storyboardFragmentAdapter);
	}

	@Override
	protected void onResume() {
		super.onResume();
		// Reset the text of the start button
		Button btnStart = (Button) findViewById(R.id.btnStartStoryboard);
		btnStart.setText(R.string.btn_startStoryboard);
	}
	
	/**
	 * Called when the show information button is clicked
	 * @return btnClickListener
	 */
	private BtnClickListener showInformationListener() {
		return 
			new BtnClickListener() {

		    @Override
		    public void onBtnClick(StoryboardFragment storyboardFragment, View view) {
		    	
		    	// custom dialog
				final Dialog dialog = new Dialog(CONTEXT);
				dialog.setContentView(R.layout.dialog_show_information);
				dialog.setTitle(storyboardFragment.getTitle());
	 
				// set the custom dialog components - text, image and button
				TextView text = (TextView) dialog.findViewById(R.id.text);
				text.setText(storyboardFragment.getDescription());
				TextView duration = (TextView) dialog.findViewById(R.id.duration);
				duration.setText("Duration: " + convertDuration(storyboardFragment.getDuration()));
				ImageView imageView = (ImageView) dialog.findViewById(R.id.image);
				setImageForImageView(imageView, storyboardFragment.getImage());
	 
				Button dialogButton = (Button) dialog.findViewById(R.id.btnClose);
				// if button is clicked, close the custom dialog
				dialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
	 
				dialog.show();
		    }

		};
	}
	
	/**
	 * Method that converts the seconds to a readable string (hours + minutes + seconds)
	 * @param durationInSeconds
	 * @return
	 */
	private String convertDuration(int durationInSeconds) {
		int hours = durationInSeconds / 3600;
		int minutes = (durationInSeconds % 3600) / 60;
		int seconds = durationInSeconds % 60;
		if(hours >= 1)
			return hours+"hrs"+minutes+"m"+seconds+"s";
		else {
			if(minutes >= 1)
				return minutes+"m"+seconds+"s";
			else
				return seconds+"s";
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.title_bar_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.home:
	            finish();
	            overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);	
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	public void startStoryboard(View view) {
		ArrayList<StoryboardFragment> fragments = storyboardFragmentAdapter.getSelectedFragments();
		Intent intent = new Intent(this, StoryboardPlayingFragmentsActivity.class);
		if(fragments.size() == 0)
			intent.putExtra(StoryboardPlayingFragmentsActivity.STORY_MESSAGE, storyboardFragments);
		else
			intent.putExtra(StoryboardPlayingFragmentsActivity.STORY_MESSAGE, fragments);
		startActivity(intent);
		overridePendingTransition(R.anim.default_activity_animation_in2, R.anim.default_activity_animation_out2);
		Button btnStart = (Button) findViewById(R.id.btnStartStoryboard);
		btnStart.setText(R.string.btn_startStoryboardLoading);
		
	}
	
	/**
	 * Helper method for loading in an image from
	 * the assets folder and setting it as the src
	 * for the provided ImageView object.
	 * @param imageView
	 * @param imageUrl
	 */
	private void setImageForImageView(ImageView imageView, String imageUrl) {
		imageUrl = imageUrl.substring(0, imageUrl.lastIndexOf('.'));
		int resID = CONTEXT.getResources().getIdentifier(imageUrl, "drawable",  CONTEXT.getPackageName());
		imageView.setImageResource(resID);
	}
}
