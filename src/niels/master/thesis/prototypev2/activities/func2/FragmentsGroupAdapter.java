package niels.master.thesis.prototypev2.activities.func2;

import java.util.ArrayList;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.entities.FactBlock;
import niels.master.thesis.prototypev2.entities.ImageBlock;
import niels.master.thesis.prototypev2.entities.MapBlock;
import niels.master.thesis.prototypev2.entities.QuizBlock;
import niels.master.thesis.prototypev2.entities.StoryboardFragment;
import niels.master.thesis.prototypev2.fragments.FactBlockFragment;
import niels.master.thesis.prototypev2.fragments.ImageBlockFragment;
import niels.master.thesis.prototypev2.fragments.MapBlockFragment;
import niels.master.thesis.prototypev2.fragments.QuizBlockFragment;
import niels.master.thesis.prototypev2.lib.FancyCoverFlow;
import niels.master.thesis.prototypev2.lib.FancyCoverFlowAdapter;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FragmentsGroupAdapter extends FancyCoverFlowAdapter {

    // =============================================================================
    // Private members
    // =============================================================================

	private ArrayList<StoryboardFragment> fragments;
	private BtnClickListener showClickListener = null;
	private BtnClickListener showExtraClickListener = null;
	private BtnClickListener playFragmentListener = null;
	
	// =============================================================================
    // Constructor
    // =============================================================================
	public FragmentsGroupAdapter(ArrayList<StoryboardFragment> fragments, 
			BtnClickListener showClickListener, BtnClickListener playFragmentListener, BtnClickListener showExtraClickListener) {
		this.fragments = fragments;
		this.showClickListener = showClickListener;
		this.playFragmentListener = playFragmentListener;
		this.showExtraClickListener = showExtraClickListener;
	}
	
	public void refresh() {
		notifyDataSetChanged();
	}
	
	// =============================================================================
    // Supertype overrides
    // =============================================================================

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Override
    public StoryboardFragment getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getCoverFlowItem(int i, View reuseableView, ViewGroup viewGroup) {
        CustomViewGroup customViewGroup = null;

        /*if (reuseableView != null) {
            customViewGroup = (CustomViewGroup) reuseableView;
        } else {*/
            customViewGroup = new CustomViewGroup(viewGroup.getContext());
            customViewGroup.setLayoutParams(new FancyCoverFlow.LayoutParams(300, 520));
        //}

        // Image
        setImageForImageView(customViewGroup.getImageView(), this.getItem(i).getImage());
        customViewGroup.getImageView().setOnTouchListener(createPlayFragmentClickListener(this.getItem(i), i));
        
        // Text
        customViewGroup.getTextView().setText(String.format(this.getItem(i).getTitle()));
        
        // button listener show information
        customViewGroup.getButton().setOnClickListener(createShowInformationOnClickListener(this.getItem(i)));
        
        // button listener Show extra information fragment
        customViewGroup.getPlayButton().setOnClickListener(createShowExtraInformationOnClickListener(this.getItem(i)));
        
        // SET THE TEXT BASED ON THE TYPE OF INFORMATION
		try {
			String text;
			text = setTextBasedOnTypeOfInformation(this.getItem(i).getInformationType());
			customViewGroup.getPlayButton().setText(text);
			customViewGroup.getPlayButton().setCompoundDrawablesWithIntrinsicBounds(0, setDrawableBasedOnTypeOfInformation(this.getItem(i).getInformationType()), 0, 0);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
        
        if(i == StoryboardPlayingFragmentsActivity.active_index && StoryboardPlayingFragmentsActivity.active_index != -1)
        	customViewGroup.getLabelView().setText(String.format("- Active -"));
        else customViewGroup.getLabelView().setText(String.format(""));
        return customViewGroup;
    }
    
    /**
     * Method that returns the drawable of the 'show information'
     * button based on the information type of the connected informationblock
     * @param type
     * @return
     * @throws Exception
     */
    private int setDrawableBasedOnTypeOfInformation(String type) throws Exception {
		if(type.equals(FactBlock.TABLE_NAME))
			return R.drawable.factblock_icon_sm;
		if(type.equals(ImageBlock.TABLE_NAME))
			return R.drawable.imageblock_icon_sm;
		if(type.equals(MapBlock.TABLE_NAME))
			return R.drawable.mapblock_icon_sm;
		if(type.equals(QuizBlock.TABLE_NAME))
			return R.drawable.quizblock_icon_sm;
		throw new Exception("FragmentsGroupAdapter - setDrawableBasedOnTypeOfInformationn - forgot to add newly added fragment");
	}
    
    /**
     * Method that returns the text of the 'show information'
     * button based on the information type of the connected informationblock
     * @param type
     * @return
     * @throws Exception
     */
    private String setTextBasedOnTypeOfInformation(String type) throws Exception {
		if(type.equals(FactBlock.TABLE_NAME))
			return "Show a fun fact";
		if(type.equals(ImageBlock.TABLE_NAME))
			return "Show unseen images";
		if(type.equals(MapBlock.TABLE_NAME))
			return "Show an interactive map";
		if(type.equals(QuizBlock.TABLE_NAME))
			return "Do a little quiz";
		throw new Exception("FragmentsGroupAdapter - setTextBasedOnTypeOfInformation - forgot to add newly added fragment");
	}
    
    /**
	 * Executed when the "Show Information" button is clicked
	 */
	private OnClickListener oCLShow = null;
	private OnClickListener createShowInformationOnClickListener(final StoryboardFragment storyboardFragment) {
		oCLShow = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showClickListener.onBtnClick(storyboardFragment, v);	
			}
		};
		return oCLShow;
	}
	
	/**
	 * Executed when the "Show extra information" button is clicked
	 */
	private OnClickListener oCLShowExtra = null;
	private OnClickListener createShowExtraInformationOnClickListener(final StoryboardFragment storyboardFragment) {
		oCLShowExtra = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showExtraClickListener.onBtnClick(storyboardFragment, v);	
			}
		};
		return oCLShowExtra;
	}
	
	/**
	 * Executed when the image is touched & clicked
	 */
	private OnTouchListener oCLPlayFragment = null;
	private OnTouchListener createPlayFragmentClickListener(final StoryboardFragment storyboardFragment, final int index) {
		oCLPlayFragment = new View.OnTouchListener() {
			
			private float actionDown = 0;
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP && Math.abs(event.getAxisValue(MotionEvent.AXIS_Y) - actionDown) == 0) {
					float t = event.getAxisValue(MotionEvent.AXIS_Y);
					System.out.println(t + " xx " + actionDown + " xx " + Math.abs(t - actionDown));
				    //this is click
					// Set as active index
					StoryboardPlayingFragmentsActivity.active_index = index;
					// call the onBtnClick method
					playFragmentListener.onBtnClick(storyboardFragment, v);
					return false;
				}
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					actionDown = event.getAxisValue(MotionEvent.AXIS_Y);
					return StoryboardPlayingFragmentsActivity.getCoverFlow().onTouchEvent(event);
				}				
				return StoryboardPlayingFragmentsActivity.getCoverFlow().onTouchEvent(event);
			}
			
		};
		return oCLPlayFragment;
				
//				new View.OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				// Set as active index
//				StoryboardPlayingFragmentsActivity.active_index = index;
//				// call the onBtnClick method
//				playFragmentListener.onBtnClick(storyboardFragment, v);	
//			}
//		};
		
	}
    
    // =============================================================================
    // Setter
    // =============================================================================
    
    /**
	 * Helper method for loading in an image from
	 * the assets folder and setting it as the src
	 * for the provided ImageView object.
	 * @param imageView
	 * @param imageUrl
	 */
	private void setImageForImageView(ImageView imageView, String imageUrl) {
		imageUrl = imageUrl.substring(0, imageUrl.lastIndexOf('.'));
		int resID = StoryboardPlayingFragmentsActivity.CONTEXT.getResources().getIdentifier(imageUrl, "drawable",  
				StoryboardPlayingFragmentsActivity.CONTEXT.getPackageName());
		Resources r = StoryboardPlayingFragmentsActivity.CONTEXT.getResources();
		Drawable[] layers = new Drawable[2];
		layers[0] = r.getDrawable(resID);
		layers[1] = r.getDrawable(R.drawable.overlay);
		LayerDrawable layerDrawable = new LayerDrawable(layers);
		imageView.setImageDrawable(layerDrawable);
		//imageView.setImageResource(resID);
	}

	private class CustomViewGroup extends LinearLayout {

        // =============================================================================
        // Child views
        // =============================================================================
    	
    	private TextView labelView;
    	
        private TextView textView;

        private ImageView imageView;

        private Button button, playButton;

        // =============================================================================
        // Constructor
        // =============================================================================

        private CustomViewGroup(Context context) {
            super(context);

            this.setOrientation(VERTICAL);

            this.labelView = new TextView(context);
            this.textView = new TextView(context);
            this.imageView = new ImageView(context);
            this.button = new Button(context);
            this.playButton = new Button(context);

            LinearLayout.LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 
            		ViewGroup.LayoutParams.MATCH_PARENT);

            this.labelView.setLayoutParams(layoutParams);
            labelView.setTextSize(16);
            this.textView.setLayoutParams(layoutParams);
            textView.setTextSize(20);
            this.imageView.setLayoutParams(layoutParams);
            this.button.setLayoutParams(layoutParams);
            this.playButton.setLayoutParams(layoutParams);

            this.labelView.setGravity(Gravity.CENTER);
            this.textView.setGravity(Gravity.CENTER);

            this.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            this.imageView.setAdjustViewBounds(true);

            this.button.setText(R.string.btn_information);
            this.playButton.setText(R.string.btn_playSingleFragment);

            this.addView(this.labelView);
            this.addView(this.button);
            this.addView(this.imageView);
            this.addView(this.textView);
            this.addView(this.playButton);
            
        }

        // =============================================================================
        // Getters
        // =============================================================================

        private TextView getTextView() {
            return textView;
        }
        
        private TextView getLabelView() {
        	return labelView;
        }

        private Button getButton() {
        	return button;
        }
        
        private Button getPlayButton() {
        	return playButton;
        }
        
        private ImageView getImageView() {
            return imageView;
        }

    }
	
}
