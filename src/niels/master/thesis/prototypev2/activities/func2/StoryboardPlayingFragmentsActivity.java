package niels.master.thesis.prototypev2.activities.func2;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.R.id;
import niels.master.thesis.prototypev2.R.layout;
import niels.master.thesis.prototypev2.R.menu;
import niels.master.thesis.prototypev2.activities.func1.HomeScreenActivity;
import niels.master.thesis.prototypev2.activities.func1.InformationBlockActivity;
import niels.master.thesis.prototypev2.activities.func1.InformationBlockExtraActivity;
import niels.master.thesis.prototypev2.activities.func1.WatchedBeforeBlockOverviewActivity;
import niels.master.thesis.prototypev2.aws.sns.MySNS;
import niels.master.thesis.prototypev2.aws.sqs.MySQS2;
import niels.master.thesis.prototypev2.entities.Documentary;
import niels.master.thesis.prototypev2.entities.StoryboardFragment;
import niels.master.thesis.prototypev2.functionality.ActivityExtender;
import niels.master.thesis.prototypev2.functionality.SynchronizationHandler;
import niels.master.thesis.prototypev2.lib.FancyCoverFlow;
import niels.master.thesis.prototypev2.lib.FancyCoverFlowAdapter;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

public class StoryboardPlayingFragmentsActivity extends ActivityExtender {
	
	public static Context CONTEXT;
	public static final String STORY_MESSAGE = "STORY_MESSAGE";
	static public ArrayList<StoryboardFragment> fragments;
	static public int active_index = 0;
	public static boolean isPlaying = false;
	public static boolean isMuted = false;
	private MySQS2 mySQS = null;
	private MySQS2 mySQSSending = null;
	private static FancyCoverFlow fancyCoverFlow;
	
	public static final String PASSED_MESSAGE = "PASSED_MESSAGE";
	
	public static final String CMD_MESSAGE_PLAY = "CMD_PLAY";
	public static final String CMD_MESSAGE_PAUSE = "CMD_PAUSE";
	public static final String CMD_MESSAGE_MUTE_ON = "CMD_MUTE_ON";
	public static final String CMD_MESSAGE_MUTE_OFF = "CMD_MUTE_OFF";
	public static final String CMD_MESSAGE_VOLUME = "CMD_VOLUME_";
	public static final String CMD_ITEM_PLAY = "CMD_ITEM_PLAY_";
	public static final String CMD_STARTUP = "CMD_STARTUP";
	public static final String CMD_STORE = "STORE,";
	
	public static int progress = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_storyboardplayingfragments);

		/**
		 * -------------------
		 * MAIN CODE EXECUTION
		 * -------------------
		 */
		CONTEXT = this;
		fullScreenMode();
		
		readIntent();
		FragmentsGroupAdapter adapter = initFragmentsGroupAdapter();
		fancyCoverFlow = initCoverFlow(adapter);
        onStartFunctionality(adapter, fancyCoverFlow);
        
        sendStartUpMessageToMediaPlayer();
		
    }
	
	@Override
	public void onResume() {
		super.onResume();
		System.out.println("REDRAWED!");
		autoPauseChecker(false);
		redrawElements();
	}
	
	/**
	 * Getter for fancycoverflow
	 * @return the coverflow
	 */
	public static FancyCoverFlow getCoverFlow(){
		return fancyCoverFlow;
	}
	
	/**
	 * Gets the intent and reads the data
	 */
	@SuppressWarnings("unchecked")
	private void readIntent() {
		//Intent receive
		Intent intent = getIntent();
		fragments = (ArrayList<StoryboardFragment>) intent.getSerializableExtra(STORY_MESSAGE);
	}
	
	private FragmentsGroupAdapter initFragmentsGroupAdapter() {
		return new FragmentsGroupAdapter(fragments, showInformationListener(), playFragmentListener(), showExtraInformationLisener());
	}
	
	/**
	 * Initializes the coverflow
	 * @return created adapter
	 */
	private FancyCoverFlow initCoverFlow(FragmentsGroupAdapter adapter) {
		// Coverflow
        FancyCoverFlow fancyCoverFlow = (FancyCoverFlow) this.findViewById(R.id.fancyCoverFlow);
        fancyCoverFlow.setAdapter(adapter);
        fancyCoverFlow.setUnselectedAlpha(1.0f);
        fancyCoverFlow.setUnselectedSaturation(0.0f);
        fancyCoverFlow.setUnselectedScale(0.5f);
        fancyCoverFlow.setMaxRotation(25);
        fancyCoverFlow.setScaleDownGravity(0.6f);
        fancyCoverFlow.setActionDistance(FancyCoverFlow.ACTION_DISTANCE_AUTO);
        return fancyCoverFlow;
        
	}
	
	private void onStartFunctionality(FragmentsGroupAdapter adapter, FancyCoverFlow fancyCoverFlow) {
		// Reset index
		active_index = 0;
		// SNS Message
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		//MySNS mySNS = new MySNS();
		// SQS sending init
		mySQSSending = new MySQS2();
		String message = "";
		int counter = 0;
		for (StoryboardFragment storyboardFragment : fragments) {
			if(counter != 0) message = message + "," + storyboardFragment.getId();
			else {
				message = message + storyboardFragment.getId();
				counter++;
			}
		}
		System.out.println(CMD_STORE+message);
		mySQSSending.sendMessageToQueue(CMD_STORE+message);
		//mySNS.publish("Playlist", message);
		
		// SQS RECEIVING
		TextView txtStatus = (TextView) findViewById(R.id.txtStatus);
		mySQS = new MySQS2(fancyCoverFlow, adapter, txtStatus);
		mySQS.receiveMessage();
	
		// Volume Seekbar
		SeekBar seekBar = (SeekBar) findViewById(R.id.sliderVolume);
		TextView textView = (TextView) findViewById(R.id.txtVolume);
		progress = seekBar.getProgress();
		textView.setText("Volume: " + progress);
	  
		seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			  TextView textView = (TextView) findViewById(R.id.txtVolume);
			  
			  @Override
			  public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
				  progress = progresValue;
				  textView.setText("Volume: " + progress);
			  }
			
			  @Override
			  public void onStartTrackingTouch(SeekBar seekBar) {
				  // do nothing special
			  }
			
			  @Override
			  public void onStopTrackingTouch(SeekBar seekBar) {
				  textView.setText("Volume: " + progress);
				  mySQSSending.sendMessageToQueue(CMD_MESSAGE_VOLUME + progress);
			  }
	    });
	}
	
	/**
	 * Redraw the screen elements
	 */
	private void redrawElements() {
		SeekBar seekBar = (SeekBar) findViewById(R.id.sliderVolume);
		TextView textView = (TextView) findViewById(R.id.txtVolume);
		textView.setText("Volume: " + StoryboardPlayingFragmentsActivity.progress);
		seekBar.setProgress(StoryboardPlayingFragmentsActivity.progress);
		ImageButton btnMute = (ImageButton) findViewById(R.id.btnMute);
		if(isMuted)
			btnMute.setImageResource(R.drawable.ic_ctrl_sound_mute);
		else btnMute.setImageResource(R.drawable.ic_ctrl_sound_active);
		ImageButton btn = (ImageButton) findViewById(R.id.btnPlay);
		if(!StoryboardPlayingFragmentsActivity.isPlaying)
			btn.setImageResource(R.drawable.ic_ctrl_play);
		else btn.setImageResource(R.drawable.ic_ctrl_pause);
		
	}
	
	/**
	 * Sends a message to the online media player
	 * which forces a refresh of the browser. 
	 * Wait 1 second before sending!
	 */
	private void sendStartUpMessageToMediaPlayer() {
		mySQSSending.sendMessageToQueue(CMD_STARTUP);
		try {
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void ctrlPrevious(View view) {
		CharSequence text = "";
		if(active_index > 0) {
			text = "Play previous fragment";
			active_index--;
			mySQSSending.sendMessageToQueue(CMD_ITEM_PLAY + active_index);
			// Automatically starts playing, so update play button
			if(!isPlaying) {
				ImageButton btn = (ImageButton) findViewById(R.id.btnPlay);
				btn.setImageResource(R.drawable.ic_ctrl_pause);
				isPlaying = !isPlaying;
			}
		}
		else {
			text = "No previous fragment";
			System.out.println("No more fragments [previous]");
		}
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(getApplicationContext(), text, duration);
		toast.show();
	}
	
	public void ctrlNext(View view) {
		CharSequence text = "";
		if(active_index < fragments.size()) {
			active_index++;
			text = "Play next fragment";
			playFragmentWithActiveIndex();
		}
		else {
			text = "No next fragment";
			System.out.println("No more fragments [next]");
		}
		
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(getApplicationContext(), text, duration);
		toast.show();
	}
	
	/**
	 * Send the message to play the fragment which index equals the active_index
	 */
	private void playFragmentWithActiveIndex() {
		mySQSSending.sendMessageToQueue(CMD_ITEM_PLAY + active_index);
		// Automatically starts playing, so update play button
		if(!isPlaying) {
			ImageButton btn = (ImageButton) findViewById(R.id.btnPlay);
			btn.setImageResource(R.drawable.ic_ctrl_pause);
			isPlaying = !isPlaying;
			isManuallyPaused = false;
		}
	}
	
	private boolean isManuallyPaused = false;
	
	public void ctrlPlay(View view) {
		ImageButton btn = (ImageButton) findViewById(R.id.btnPlay);
		CharSequence text = "";
		// If pause button was showed before clicking
		if(isPlaying) {
			text = "Pause";
			btn.setImageResource(R.drawable.ic_ctrl_play);
			mySQSSending.sendMessageToQueue(CMD_MESSAGE_PAUSE);
			isManuallyPaused = true;
		}
		// Else play button was showed
		else {
			text = "Play";
			btn.setImageResource(R.drawable.ic_ctrl_pause);
			mySQSSending.sendMessageToQueue(CMD_MESSAGE_PLAY);
			isManuallyPaused = false;
			// send sqs message
		}
		isPlaying = !isPlaying;
		
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(getApplicationContext(), text, duration);
		toast.show();
	}
	
	public void ctrlMute(View view) {
		CharSequence text = "";
		ImageButton btn = (ImageButton) findViewById(R.id.btnMute);
		// If audio button was showed before clicking
		//  we are going to mute the audio
		if(!isMuted) {
			text = "Audio muted";
			btn.setImageResource(R.drawable.ic_ctrl_sound_mute);
			// Volume Seekbar
			SeekBar seekBar = (SeekBar) findViewById(R.id.sliderVolume);
			TextView textView = (TextView) findViewById(R.id.txtVolume);
			seekBar.setProgress(0);
			textView.setText("Volume: 0");
			// send sqs message
			mySQSSending.sendMessageToQueue(CMD_MESSAGE_MUTE_ON);
		}
		// Else unmute button was showed
		else {
			text = "Audio active";
			btn.setImageResource(R.drawable.ic_ctrl_sound_active);
			// Volume Seekbar
			SeekBar seekBar = (SeekBar) findViewById(R.id.sliderVolume);
			TextView textView = (TextView) findViewById(R.id.txtVolume);
			seekBar.setProgress(100);
			textView.setText("Volume: 100");
			// send sqs message
			mySQSSending.sendMessageToQueue(CMD_MESSAGE_MUTE_OFF);
		}
		isMuted = !isMuted;
		int duration = Toast.LENGTH_SHORT;
		Toast toast = Toast.makeText(getApplicationContext(), text, duration);
		toast.show();
	}
	
	public void ctrlEdit(View view) {
		//Intent intent = new Intent(this, StoryboardActivity.class);
		//intent.putExtra(WatchedBeforeBlockOverviewActivity.DOC_MESSAGE, Documentary.CURRENT_DOC_ID);
		//startActivity(intent);
		mySQS.setActive(false);
		this.finish();
		overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);
	}
    
	/**
	 * Checks if autoPause is actived
	 * If so, acts accordingly
	 */
	private void autoPauseChecker(boolean isOpeningInformation) {
		if(autoPause == 1) {
			if(isPlaying) {
				if(isOpeningInformation) {
					ImageButton btn = (ImageButton) findViewById(R.id.btnPlay);
					btn.setImageResource(R.drawable.ic_ctrl_play);
					mySQSSending.sendMessageToQueue(CMD_MESSAGE_PAUSE);
					isPlaying = !isPlaying;
				}
			}
			else {
				if(!isOpeningInformation && !isManuallyPaused) {
					ImageButton btn = (ImageButton) findViewById(R.id.btnPlay);
					btn.setImageResource(R.drawable.ic_ctrl_pause);
					mySQSSending.sendMessageToQueue(CMD_MESSAGE_PLAY);
					isPlaying = !isPlaying;
				}
			}
		}
	}
	
	/**
	 * Called when the show information button is clicked
	 * @return btnClickListener
	 */
	private BtnClickListener showInformationListener() {
		return 
			new BtnClickListener() {

		    @Override
		    public void onBtnClick(StoryboardFragment storyboardFragment, View view) {
		    	
		    	// custom dialog
				final Dialog dialog = new Dialog(CONTEXT);
				dialog.setContentView(R.layout.dialog_show_information);
				dialog.setTitle(storyboardFragment.getTitle());
	 
				// set the custom dialog components - text, image and button
				TextView text = (TextView) dialog.findViewById(R.id.text);
				text.setText(storyboardFragment.getDescription());
				TextView duration = (TextView) dialog.findViewById(R.id.duration);
				duration.setText("Duration: " + convertDuration(storyboardFragment.getDuration()));
				ImageView imageView = (ImageView) dialog.findViewById(R.id.image);
				setImageForImageView(imageView, storyboardFragment.getImage());
	 
				Button dialogButton = (Button) dialog.findViewById(R.id.btnClose);
				// if button is clicked, close the custom dialog
				dialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						dialog.dismiss();
					}
				});
	 
				dialog.show();
		    }

		};
	}
	
	private BtnClickListener showExtraInformationLisener() {
		return 
			new BtnClickListener() {
			
			@Override
			public void onBtnClick(StoryboardFragment storyboardFragment, View view) {
				// Pause if autoPause == 1
				autoPauseChecker(true);
				// Launch informationblock screen
				Intent intent = new Intent(CONTEXT, InformationBlockExtraActivity.class);
				String[] message = {storyboardFragment.getInformationId() + "", storyboardFragment.getInformationType()};
				intent.putExtra(PASSED_MESSAGE, message);
				startActivity(intent);
				overridePendingTransition(R.anim.default_activity_animation_in2, R.anim.default_activity_animation_out2);
			}
		};
	}
	
	/**
	 * Called when the "play this fragment" button is clicked
	 * @return btnClickListener
	 */
	private BtnClickListener playFragmentListener() {
		return new BtnClickListener() {
			
			@Override
			public void onBtnClick(StoryboardFragment storyboardFragment, View view) {
				CharSequence text = "Play fragment '" + storyboardFragment.getTitle()+"'";
				int duration = Toast.LENGTH_SHORT;
				Toast toast = Toast.makeText(getApplicationContext(), text, duration);
				toast.show();
				playFragmentWithActiveIndex();
			}
		};
	}
	
    /**
	 * Helper method for loading in an image from
	 * the assets folder and setting it as the src
	 * for the provided ImageView object.
	 * @param imageView
	 * @param imageUrl
	 */
	private void setImageForImageView(ImageView imageView, String imageUrl) {
		imageUrl = imageUrl.substring(0, imageUrl.lastIndexOf('.'));
		int resID = CONTEXT.getResources().getIdentifier(imageUrl, "drawable",  CONTEXT.getPackageName());
		imageView.setImageResource(resID);
	}
    
	/**
	 * Method that converts the seconds to a readable string (hours + minutes + seconds)
	 * @param durationInSeconds
	 * @return
	 */
	private String convertDuration(int durationInSeconds) {
		int hours = durationInSeconds / 3600;
		int minutes = (durationInSeconds % 3600) / 60;
		int seconds = durationInSeconds % 60;
		if(hours >= 1)
			return hours+"hrs"+minutes+"m"+seconds+"s";
		else {
			if(minutes >= 1)
				return minutes+"m"+seconds+"s";
			else
				return seconds+"s";
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    // Inflate the menu items for use in the action bar
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.title_bar_storyboard_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle presses on the action bar items
	    switch (item.getItemId()) {
	        case R.id.home:
	        	// Pause documentary
	        	mySQSSending.sendMessageToQueue(CMD_MESSAGE_PAUSE);
	    		Intent intent = new Intent(this, HomeScreenActivity.class);
	    		intent.putExtra(HomeScreenActivity.MODE_MESSAGE, true);
	    		startActivity(intent);
	            overridePendingTransition(R.anim.default_activity_animation_in, R.anim.default_activity_animation_out);	
	            mySQS.setActive(false);
	            mySQSSending.setActive(false);
	            finish();
	            return true;
	        case R.id.action_settings:
	        	showDialog();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private int autoPause = 0; 
	private int buffKey = 0; // add buffer value 
	
	/**
	 * Method that shows the dialog
	 */
	private void showDialog() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(StoryboardPlayingFragmentsActivity.CONTEXT);
		
		// set title
		alertDialogBuilder.setTitle("Automatically pause when viewing extra information?");

		// set dialog message
		//alertDialogBuilder.setMessage("With this setting, to avoid distraction from the documentary you're watching, you can configure the "
			//		+ "application to automatically pause the documentary everytime you decide to view extra information. "
				//	+ "Do you want to automatically pause the documentary when viewing extra information?");

		//alertDialogBuilder.setMessage("Do you want to automatically pause the documentary when viewing extra information?");
		
		final CharSequence[] choiceList = 
	    {"No", "Yes"};
	    
		alertDialogBuilder.setSingleChoiceItems(
	            choiceList, 
	            autoPause, 
	            new DialogInterface.OnClickListener() {
	         
	        @Override
	        public void onClick(
	                DialogInterface dialog, 
	                int which) {
	            //set to buffKey instead of selected 
	            //(when cancel not save to selected)
	            buffKey = which;
	        }
	    })
	    .setCancelable(false)
	    .setPositiveButton("Save", 
	        new DialogInterface.OnClickListener() 
	        {
	            @Override
	            public void onClick(DialogInterface dialog, 
	                    int which) {
	                //set buff to selected
	                autoPause = buffKey;
	            }
	        }
	    )
	    .setNegativeButton("Cancel", 
	        new DialogInterface.OnClickListener() 
	        {
	            @Override
	            public void onClick(DialogInterface dialog, 
	                    int which) {
	            		// do nothing
	            }
	        }
	    );		

		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}
}
