package niels.master.thesis.prototypev2.fragments;

import java.io.IOException;
import java.io.InputStream;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.activities.func1.HomeScreenActivity;
import niels.master.thesis.prototypev2.activities.func1.InformationBlockActivity;
import niels.master.thesis.prototypev2.database.DatabaseAdapter;
import niels.master.thesis.prototypev2.entities.ImageBlock;
import niels.master.thesis.prototypev2.entities.InformationBlock;
import niels.master.thesis.prototypev2.functionality.SynchronizationHandler;
import android.app.Fragment;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageBlockFragment extends Fragment implements IFragment<ImageBlock> {

	private static final String IMAGE_PATH = "images/";
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {	
		// Inflate view
		View inflatedView = inflater.inflate(R.layout.fragment_image_block, container, false);
		// GET ID FROM PASSED INTENT ARGUMENTS
		String[] messageReceived = getArguments().getStringArray(SynchronizationHandler.PASSED_MESSAGE);
		int id = Integer.parseInt(messageReceived[0]);
		ImageBlock informationBlock = loadData(id);
		// SET TEXT
		fillGUIWithData(informationBlock, inflatedView);
        // Inflate the layout for this fragment
        return inflatedView;
    }

	@Override
	public ImageBlock loadData(int id) {
		String tableName = ImageBlock.TABLE_NAME;
		DatabaseAdapter dba = new DatabaseAdapter(HomeScreenActivity.CONTEXT);
		dba.open();
		InformationBlock informationBlock = 
				dba.getSingleInformationBlockById(id, tableName, InformationBlock.getColumns(tableName));
		dba.close();
		return (ImageBlock) informationBlock;
	}

	@Override
	public void fillGUIWithData(ImageBlock informationBlock, View view) {
		TextView titleInformationBlock = (TextView) view.findViewById(R.id.titleInformationBlock);
		TextView descriptionInformationBlock = (TextView) view.findViewById(R.id.descriptionInformationBlock);
		ImageView imageView1 = (ImageView) view.findViewById(R.id.image1);
		ImageView imageView2 = (ImageView) view.findViewById(R.id.image2);
		ImageView imageView3 = (ImageView) view.findViewById(R.id.image3);
		titleInformationBlock.setText(informationBlock.getTitle());
		descriptionInformationBlock.setText(informationBlock.getText());
		setImageForImageView(imageView1, informationBlock.getImage1Url());
		if(informationBlock.getImage2Url() != null) {
			if(!informationBlock.getImage2Url().equals(""))
				setImageForImageView(imageView2, informationBlock.getImage2Url());
		}
		if(informationBlock.getImage3Url() != null) {
			if(!informationBlock.getImage3Url().equals(""))
				setImageForImageView(imageView3, informationBlock.getImage3Url());
		}
	}
	
	/**
	 * Helper method for loading in an image from
	 * the assets folder and setting it as the src
	 * for the provided ImageView object.
	 * @param imageView
	 * @param imageUrl
	 */
	private void setImageForImageView(ImageView imageView, String imageUrl) {
		imageUrl = imageUrl.substring(0, imageUrl.lastIndexOf('.'));
		int resID = getResources().getIdentifier(imageUrl, "drawable",  getActivity().getPackageName());
		imageView.setImageResource(resID);
//		Resources res = getResources();
//		AssetManager am = res.getAssets();
//		InputStream iS = null;
//		try {
//			iS = am.open(IMAGE_PATH + imageUrl);
//			BitmapFactory.Options options = new BitmapFactory.Options();
//			options.inSampleSize = 2;
//			imageView.setImageBitmap(BitmapFactory.decodeStream(iS, null, options));   
//		} catch (IOException e) {
//			Log.e(null, "Failed to load image for ImageBlock - ImageBlockFragment");
//		} finally {
//	        //Always clear and close
//	        try {
//	            iS.close();
//	            iS = null;
//	        } catch (IOException e) {
//	        	Log.e(null, "Failed to close Inputstream - ImageBlockFragment");
//	        }
//		}
	}
	
}
