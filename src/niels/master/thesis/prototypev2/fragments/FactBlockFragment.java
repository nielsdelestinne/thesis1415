package niels.master.thesis.prototypev2.fragments;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.activities.func1.HomeScreenActivity;
import niels.master.thesis.prototypev2.activities.func1.InformationBlockActivity;
import niels.master.thesis.prototypev2.database.DatabaseAdapter;
import niels.master.thesis.prototypev2.entities.FactBlock;
import niels.master.thesis.prototypev2.entities.InformationBlock;
import niels.master.thesis.prototypev2.functionality.SynchronizationHandler;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FactBlockFragment extends Fragment implements IFragment<FactBlock> {

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
		
		// Inflate view
		View inflatedView = inflater.inflate(R.layout.fragment_fact_block, container, false);
		// GET ID FROM PASSED INTENT ARGUMENTS
		String[] messageReceived = getArguments().getStringArray(SynchronizationHandler.PASSED_MESSAGE);
		int id = Integer.parseInt(messageReceived[0]);
		FactBlock informationBlock = loadData(id);
		// SET TEXT
		fillGUIWithData(informationBlock, inflatedView);
        // Inflate the layout for this fragment
        return inflatedView;
    }

	@Override
	public FactBlock loadData(int id) {
		String tableName = FactBlock.TABLE_NAME;
		DatabaseAdapter dba = new DatabaseAdapter(HomeScreenActivity.CONTEXT);
		dba.open();
		InformationBlock informationBlock = 
				dba.getSingleInformationBlockById(id, tableName, InformationBlock.getColumns(tableName));
		dba.close();
		return (FactBlock) informationBlock;
	}

	@Override
	public void fillGUIWithData(FactBlock informationBlock, View view) {
		TextView titleInformationBlock = (TextView) view.findViewById(R.id.titleInformationBlock);
		TextView descriptionInformationBlock = (TextView) view.findViewById(R.id.descriptionInformationBlock);
		titleInformationBlock.setText(informationBlock.getTitle());
		descriptionInformationBlock.setText(informationBlock.getText());
	}
	
}
