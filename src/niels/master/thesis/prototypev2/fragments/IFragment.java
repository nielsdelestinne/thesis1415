package niels.master.thesis.prototypev2.fragments;

import android.view.View;
import niels.master.thesis.prototypev2.entities.InformationBlock;

public interface IFragment<IB extends InformationBlock> {

	/**
	 * Retrieves the record identified by "id" from the table (as specified in the
	 * class implementing this method) and creates an InformationBlock (subclass)
	 * object with it, which is returned.
	 * @param id
	 * @return A InformationBlock (subclass) object
	 */
	IB loadData(int id);
	
	/**
	 * Method that uses the data from the InformationBlock object 
	 * "informationBlock" to set the content of the GUI.
	 * @param informationBlock
	 */
	void fillGUIWithData(IB informationBlock, View view);
	
}
