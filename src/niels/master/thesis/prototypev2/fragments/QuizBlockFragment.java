package niels.master.thesis.prototypev2.fragments;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.activities.func1.HomeScreenActivity;
import niels.master.thesis.prototypev2.activities.func1.InformationBlockActivity;
import niels.master.thesis.prototypev2.activities.func1.SimpleInformationBlockActivity;
import niels.master.thesis.prototypev2.database.DatabaseAdapter;
import niels.master.thesis.prototypev2.entities.InformationBlock;
import niels.master.thesis.prototypev2.entities.QuizBlock;
import niels.master.thesis.prototypev2.functionality.SynchronizationHandler;
import niels.master.thesis.prototypev2.lib.TouchImageView;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class QuizBlockFragment extends Fragment implements IFragment<QuizBlock> {

	private static final String IMAGE_PATH = "images/";
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {	
		// Inflate view
		View inflatedView = inflater.inflate(R.layout.fragment_quiz_block, container, false);
		// GET ID FROM PASSED INTENT ARGUMENTS
		String[] messageReceived = getArguments().getStringArray(SynchronizationHandler.PASSED_MESSAGE);
		int id = Integer.parseInt(messageReceived[0]);
		QuizBlock informationBlock = loadData(id);
		// SET TEXT
		fillGUIWithData(informationBlock, inflatedView);
        // Inflate the layout for this fragment
        return inflatedView;
    }

	@Override
	public QuizBlock loadData(int id) {
		String tableName = QuizBlock.TABLE_NAME;
		DatabaseAdapter dba = new DatabaseAdapter(HomeScreenActivity.CONTEXT);
		dba.open();
		InformationBlock informationBlock = 
				dba.getSingleInformationBlockById(id, tableName, InformationBlock.getColumns(tableName));
		dba.close();
		return (QuizBlock) informationBlock;
	}
	
	@Override
	public void fillGUIWithData(QuizBlock informationBlock, View view) {
		// Gui
		TextView titleInformationBlock = (TextView) view.findViewById(R.id.titleInformationBlock);
		TextView descriptionInformationBlock = (TextView) view.findViewById(R.id.descriptionInformationBlock);
		descriptionInformationBlock.setText(informationBlock.getText());
		TouchImageView choice1 = (TouchImageView) view.findViewById(R.id.choice1);
		TouchImageView choice2 = (TouchImageView) view.findViewById(R.id.choice2);
		TouchImageView choice3 = (TouchImageView) view.findViewById(R.id.choice3);
		// CONNECT THE CUSTOM ONCLICKLISTENER
		choice1.setOnClickListener(new onAnswerClickListener(informationBlock.getChoice1Answer(), 
				informationBlock.getTitle()));
		choice2.setOnClickListener(new onAnswerClickListener(informationBlock.getChoice2Answer(), 
				informationBlock.getTitle()));
		choice3.setOnClickListener(new onAnswerClickListener(informationBlock.getChoice3Answer(), 
				informationBlock.getTitle()));
		// SET THE CONTENT
		titleInformationBlock.setText(informationBlock.getTitle());
		//descriptionInformationBlock.setText(informationBlock.getText());
		setImageForImageView(choice1, informationBlock.getChoice1());
		setImageForImageView(choice2, informationBlock.getChoice2());
		setImageForImageView(choice3, informationBlock.getChoice3());
	}
	
	/**
	 * Helper method for loading in an image from
	 * the assets folder and setting it as the src
	 * for the provided ImageView object.
	 * @param imageView
	 * @param imageUrl
	 */
	private void setImageForImageView(ImageView imageView, String imageUrl) {
		String imageUrlWithoutExtension = imageUrl.substring(0, imageUrl.lastIndexOf('.'));
		int resID = getResources().getIdentifier(imageUrlWithoutExtension, "drawable",  getActivity().getPackageName());
		imageView.setTag(imageUrl);
		imageView.setImageResource(resID);
	}
	
	/**
	 * Click listener that registers a click
	 * on one of the 3 possible answers
	 * @author Niels
	 *
	 */
	private class onAnswerClickListener implements OnClickListener{

		private String correctAnswer;
		private String title;
		
		public onAnswerClickListener(String correctAnswer, String title) {
			this.correctAnswer = correctAnswer;
			this.title = title;
		}
		
		@Override
		public void onClick(View v) {
			showDialog(correctAnswer, v.getContext());
		}
		
		/**
		 * Method that shows the dialog
		 * @param isCorrect
		 */
		private void showDialog(String correctAnswer, Context context) {
			AlertDialog.Builder alertDialogBuilder;
			alertDialogBuilder = new AlertDialog.Builder(context);

			int button;
			button = R.string.btn_correct;
		
			// set title
			alertDialogBuilder.setTitle(title);

			// set dialog message
			alertDialogBuilder.setMessage(correctAnswer);
			alertDialogBuilder	
				.setCancelable(false)
				.setNegativeButton(button,new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						// if this button is clicked, just close
						// the dialog box and do nothing
						dialog.cancel();
					}
			});

			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it
			alertDialog.show();
		}
		
	}
	
}
