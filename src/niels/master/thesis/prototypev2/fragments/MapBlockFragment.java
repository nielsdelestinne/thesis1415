package niels.master.thesis.prototypev2.fragments;

import niels.master.thesis.prototypev2.R;
import niels.master.thesis.prototypev2.activities.func1.HomeScreenActivity;
import niels.master.thesis.prototypev2.activities.func1.InformationBlockActivity;
import niels.master.thesis.prototypev2.database.DatabaseAdapter;
import niels.master.thesis.prototypev2.entities.FactBlock;
import niels.master.thesis.prototypev2.entities.InformationBlock;
import niels.master.thesis.prototypev2.entities.MapBlock;
import niels.master.thesis.prototypev2.functionality.SynchronizationHandler;
import niels.master.thesis.prototypev2.lib.TouchImageView;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class MapBlockFragment extends Fragment implements IFragment<MapBlock>{

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {	
		// Inflate view
		View inflatedView = inflater.inflate(R.layout.fragment_map_block, container, false);
		// GET ID FROM PASSED INTENT ARGUMENTS
		String[] messageReceived = getArguments().getStringArray(SynchronizationHandler.PASSED_MESSAGE);
		int id = Integer.parseInt(messageReceived[0]);
		MapBlock informationBlock = loadData(id);
		// SET TEXT
		fillGUIWithData(informationBlock, inflatedView);
        // Inflate the layout for this fragment
        return inflatedView;
    }

	@Override
	public MapBlock loadData(int id) {
		String tableName = MapBlock.TABLE_NAME;
		DatabaseAdapter dba = new DatabaseAdapter(HomeScreenActivity.CONTEXT);
		dba.open();
		InformationBlock informationBlock = 
				dba.getSingleInformationBlockById(id, tableName, InformationBlock.getColumns(tableName));
		dba.close();
		return (MapBlock) informationBlock;
	}

	@Override
	public void fillGUIWithData(MapBlock informationBlock, View view) {
		TextView titleInformationBlock = (TextView) view.findViewById(R.id.titleInformationBlock);
		//TextView descriptionInformationBlock = (TextView) view.findViewById(R.id.descriptionInformationBlock);
		TouchImageView imageView1 = (TouchImageView) view.findViewById(R.id.image1);
		titleInformationBlock.setText(informationBlock.getTitle());
		//descriptionInformationBlock.setText(informationBlock.getText());
		setImageForImageView(imageView1, informationBlock.getMapUrl());
	}
	
	/**
	 * Helper method for loading in an image from
	 * the assets folder and setting it as the src
	 * for the provided ImageView object.
	 * @param imageView
	 * @param imageUrl
	 */
	private void setImageForImageView(TouchImageView imageView, String imageUrl) {
		imageUrl = imageUrl.substring(0, imageUrl.lastIndexOf('.'));
		int resID = getResources().getIdentifier(imageUrl, "drawable",  getActivity().getPackageName());
		imageView.setImageResource(resID);
	}
	
}
